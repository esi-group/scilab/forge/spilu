// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function buildSrc()
    src_dir = get_absolute_file_path("builder_src.sce");
    tbx_builder_src_lang("fortran", src_dir);
    tbx_builder_src_lang("c", src_dir);
endfunction

buildSrc();
clear buildSrc;
