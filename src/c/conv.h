//
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This toolbox is released under the terms of the CeCILL license :
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

#ifndef _SPILUC_H_
#define _SPILUC_H_

#ifdef _MSC_VER
    #if LIBSPILUC_EXPORTS
        #define SPILUC_IMPORTEXPORT __declspec (dllexport)
    #else
        #define SPILUC_IMPORTEXPORT __declspec (dllimport)
    #endif
#else
    #define SPILUC_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

#include "scisparse.h"

__BEGIN_DECLS

// Convert the Scilab sparse format into Sparskit format
SPILUC_IMPORTEXPORT void spiluc_Sci2spk(SciSparse A, int * ia);

// Convert the Sparskit sparse format into Scilab format
SPILUC_IMPORTEXPORT int spiluc_lband(SciSparse A);
SPILUC_IMPORTEXPORT double spiluc_eltm(SciSparse A);
SPILUC_IMPORTEXPORT int spiluc_spluget(int n,int *ju,int *jlu,double *alu, SciSparse **L,SciSparse **U);

__END_DECLS

#endif /* _SPILUC_H_ */

