//
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This toolbox is released under the terms of the CeCILL license :
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

#ifndef _SPILUF_H_
#define _SPILUF_H_

#ifdef _MSC_VER
	#if LIBSPILUF_EXPORTS 
		#define SPILUF_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPILUF_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPILUF_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS


SPILUF_IMPORTEXPORT void F2C(ilu0)(int * n, double * a, int * ja, int * ia, double * alu, int * jlu, int * ju, int * iw, int * ierr);
SPILUF_IMPORTEXPORT void F2C(ilut)(int * n, double * a, int * ja, int * ia, int * lfil, double * droptol, double * alu, int * jlu, int * ju, int * iwk, double * w, int * jw, int * ierr);
SPILUF_IMPORTEXPORT void F2C(ilutp)(int * n, double * a, int * ja, int * ia, int * lfil,double * droptol, double * permtol, int * mbloc, double * alu, int * jlu, int * ju, int * iwk, double * w, int * jw, int * iperm, int * ierr);
SPILUF_IMPORTEXPORT void F2C(iluk)(int * n, double * a, int * ja, int * ia, int * lfil, double * alu, int * jlu, int * ju, int * levs, int * iwk, double * w, int * jw, int * ierr);
SPILUF_IMPORTEXPORT void F2C(ilud)(int * n, double * a, int * ja, int * ia, double * alph, double * tol,double * alu, int * jlu, int * ju, int * iwk, double * w, int * jw, int * ierr);
SPILUF_IMPORTEXPORT void F2C(iludp)(int * n, double * a, int * ja, int * ia, double * alph, double * droptol, double * permtol, int * mbloc, double * alu, int * jlu, int * ju, int * iwk, double * w, int * jw, int * iperm, int * ierr);
SPILUF_IMPORTEXPORT void F2C(milu0)(int * n, double * a, int * ja, int * ia, double * alu, int * jlu, int * ju, int * iw, int * ierr);
SPILUF_IMPORTEXPORT void F2C(lusol)(int * n, double * y, double * x, double * alu, int * jlu, int * ju);
SPILUF_IMPORTEXPORT void F2C(lutsol)(int * n, double * y, double * x, double * alu, int * jlu, int * ju);

__END_DECLS

#endif /* _SPILUF_H_ */

