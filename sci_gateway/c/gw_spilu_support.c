/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

// From ANSI C
#include "stdio.h"
#include "float.h"
#include "stdlib.h"
#include "stdio.h"

/*API scilab includes*/
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "scisparse.h"

/*Gateway includes*/
#include "gw_spilu_support.h"

// From this lib
#include "conv.h"

// Private functions
int spilu_Double2IntegerArgument(
        char * fname, void * pvApiCtx,
        int ivar,
        double dvalue, int * ivalue);

// Get a scalar double.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// rhs (input): the number of input arguments
// defaultdouble (input): the default value of the double.
// mydouble (output): the required double
//
// Description
// If there is a problem, produces an error and return 0.
// If there is no problem, returns 1.
//
// If rhs < ivar, then set the default value.
// If rhs >= ivar, and the value is [], then set the default value.
// If not, then set the actual value.
//
int spilu_getScalarDouble(
        char *fname, void * pvApiCtx,
        int ivar, int rhs,
        double defaultdouble, double * mydouble)
{
    SciErr sciErr;
    int iErr;
    int * addr = NULL;
    double value;

    /* check argument was given */
    if (Rhs >= ivar)
    {
        /* get variable address */
        sciErr = getVarAddressFromPosition(pvApiCtx, ivar, &addr);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }
        /* check it's a double matrix */
        if (isDoubleType(pvApiCtx, addr) == FALSE)
        {
            Scierror(501, _("%s: Wrong type for argument %d: "
                            "Matrix expected.\n"),
                     fname, ivar);
            return 0;
        }
        /* default if [] */
        if (isEmptyMatrix(pvApiCtx, addr))
        {
            *mydouble = defaultdouble;
        }
        else
        {
            /* check it's a scalar */
            if (isScalar(pvApiCtx, addr) == FALSE)
            {
                Scierror(501, _("%s: Wrong size for input argument #%d: "
                                "%d-by-%d matrix expected.\n"),
                         fname, ivar, 1, 1);
                return 0;
            }
            /* get value */
            iErr = getScalarDouble(pvApiCtx, addr, &value);
            if (iErr)
            {
                return 0;
            }
            *mydouble = value;
        }
    } else {
        *mydouble = defaultdouble;
    }
    return 1;
}

// Check that a double is in a given range
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// mydouble (input): the double to check
// mindouble (input): the minimum value of the double.
// maxdouble (input): the maximum value of the double.
//
// Description
// If myint is not in range, produce an error and returns 1.
// If myint is OK, returns 0.
//
int spilu_checkDoubleInRange(
        char * fname, void * pvApiCtx,
        int ivar,
        double mydouble, double mindouble, double maxdouble)
{
    if ( mydouble > maxdouble )
    {
        Scierror(204, _("%s: Wrong value for input argument #%d: "
                        "Must be < %f.\n"),
                 fname, ivar, maxdouble);
        return 0;
    }
    if ( mydouble < mindouble )
    {
        Scierror(204, _("%s: Wrong value for input argument #%d: "
                        "Must be > %f.\n"),
                 fname, ivar, mindouble);
        return 0;
    }
    return 1;
}

// Get a scalar integer from a Double.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// rhs (input): the number of input arguments
// defaultint (input): the default value of the int.
// myint (output): the required int
//
// Description
// If there is a problem, produces an error and return 0.
// If there is no problem, returns 1.
//
// If rhs < ivar, then set the default value.
// If rhs >= ivar, and the value is [], then set the default value.
// If not, then set the actual value.
//
int spilu_getScalarIntegerFromScalarDouble(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        int defaultint, int * myint)
{
    SciErr sciErr;
    int iErr;
    int * addr = NULL;
    double value;
    int readFlag;

    /* check argument was given */
    if (rhs >= ivar)
    {
        /* get variable address */
        sciErr = getVarAddressFromPosition(pvApiCtx, ivar, &addr);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }
        /* check it's a double matrix */
        if (isDoubleType(pvApiCtx, addr) == FALSE)
        {
            Scierror(501, _("%s: Wrong type for argument %d: "
                            "Matrix expected.\n"),
                     fname, ivar);
            return 0;
        }
        /* default if [] */
        if (isEmptyMatrix(pvApiCtx, addr))
        {
            *myint = defaultint;
        }
        else
        {
            /* check it's a scalar */
            if (isScalar(pvApiCtx, addr) == FALSE)
            {
                Scierror(501, _("%s: Wrong size for input argument #%d: "
                                "%d-by-%d matrix expected.\n"),
                         fname, ivar, 1, 1);
                return 0;
            }
            /* get value */
            iErr = getScalarDouble(pvApiCtx, addr, &value);
            if (iErr)
            {
                return 0;
            }
            readFlag = spilu_Double2IntegerArgument(
                    fname, pvApiCtx, ivar, value, myint);
            if ( readFlag == FALSE )
            {
                return 0;
            }
        }
    }
    else
    {
        *myint = defaultint;
    }
    return 1;
}

// Check that an integer is in a given range
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// myint (input): the integer to check
// minint (input): the minimum value of the integer.
// maxint (input): the maximum value of the integer.
//
// Description
// If myint is not in range, produce an error and returns 1.
// If myint is OK, returns 0.
//
int spilu_checkIntegerInRange(
        char * fname, void * pvApiCtx,
        int ivar,
        int myint, int minint, int maxint)
{
    if ( myint > maxint )
    {
        Scierror(204, _("%s: Wrong value for input argument #%d: "
                        "Must be < %d.\n"),
                 fname, ivar, maxint);
        return 0;
    }
    if ( myint < minint )
    {
        Scierror(204, _("%s: Wrong value for input argument #%d: "
                        "Must be > %d.\n"),
                 fname, ivar, minint);
        return 0;
    }
    return 1;
}

//
// splspc_Double2IntegerArgument --
//   Convert the given double into an integer.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   dvalue (input) : the source double value
//   ivalue (output) : the target integer value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that the double is in the integer range.
// Check that the double has no fractional part.
//
int spilu_Double2IntegerArgument(
        char * fname, void * pvApiCtx,
        int ivar,
        double dvalue, int * ivalue)
{
    if ( dvalue > INT_MAX ) {
        Scierror(999, _("%s: Too large integer value in argument #%d: "
                        "found %e while maximum value is %d.\n"),
                 fname, ivar, dvalue, INT_MAX);
        return 0;
    }
    if ( dvalue < INT_MIN ) {
        Scierror(999, _("%s: Too large integer value in argument #%d: "
                        "found %e while minimum value is %d.\n"),
                fname, ivar, dvalue, INT_MIN);
        return 0;
    }
    *ivalue = (int)dvalue;
    // Now check that the double was really an integer, with
    // a zero fractionnal part.
    if ( (double)*ivalue != dvalue ) {
        Scierror(999, _("%s: Wrong integer value in argument #%d: "
                        "found %e which is different from "
                        "the closest integer %d.\n"),
                 fname, ivar, dvalue, *ivalue);
        return 0;
    }
    return 1;
}

int spilu_getSparseMatrix(
        char * fname, void * pvApiCtx,
        int ivar, SciSparse * A)
{
    int * A_addr   = NULL; // sparse matrix A address
    int   A_m      = 0;    // sparse matrix A num rows
    int   A_n      = 0;    // sparse matrix A num cols
    int   A_nnz    = 0;    // sparse matrix A number non-zero
    int * A_nnzpr  = NULL; // sparse matrix A number non-zero per row
    int * A_colpos = NULL; // sparse matrix A column positions
    double * A_R   = NULL; // sparse matrix A real payload
    double * A_I   = NULL; // sparse matrix A imaginary payload

    SciErr sciErr;
    int iErr;

    /* initialize pointers of A to NULL
     * (avoid double free in case of error) */
    A->mnel = NULL;
    A->nel  = NULL;
    A->I    = NULL;
    A->R    = NULL;

    /* get var address */
    sciErr = getVarAddressFromPosition(pvApiCtx, ivar, &A_addr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    /* check type */
    if ( isSparseType(pvApiCtx, A_addr) == FALSE )
    {
        Scierror(501, "%s: input matrix must be sparse.\n", fname);
        puts("input matrix must be sparse");
        return 0;
    }

    /* check complexity */
    if ( isVarComplex(pvApiCtx, A_addr) == FALSE )
    {
        /* get var (complex) */
        iErr = getAllocatedSparseMatrix(
                pvApiCtx, A_addr,
                &A_m, &A_n, &A_nnz, &A_nnzpr, &A_colpos, &A_R);
        if (iErr)
        {
            freeAllocatedSparseMatrix(A_nnzpr, A_colpos, A_R);
            return 0;
        }
        A->it = 0; // real
    }
    else
    {
        /* get var (real) */
        iErr = getAllocatedComplexSparseMatrix(
                pvApiCtx, A_addr,
                &A_m, &A_n, &A_nnz, &A_nnzpr, &A_colpos, &A_R, &A_I);
        if (iErr)
        {
            freeAllocatedComplexSparseMatrix(A_nnzpr, A_colpos, A_R, A_I);
            return 0;
        }
        A->it = 1; // complex
        A->I = A_I; // imaginary payload
    }

    /* copy complexity - independent content to SciSparse struct */
    A->m    = A_m;
    A->n    = A_n;
    A->nel  = A_nnz;
    A->mnel = A_nnzpr;
    A->icol = A_colpos;
    A->R    = A_R;

    return 1;
}

//
// spilu_getArgumentDrop --
//   Get the drop parameter.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   rhs (input): the number of input arguments
//   A (input) : the sparse matrix
//   drop (output) : the value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that drop is positive.
// If drop is not provided or is equal to [], then
// set the default value, drop=0.001*max(abs(A)).
//
int spilu_getArgumentDrop(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        SciSparse A, double * drop)
{
    double defaultdrop;
    int readFlag;

    defaultdrop = 0.001*spiluc_eltm(A);
    readFlag = spilu_getScalarDouble(
            fname, pvApiCtx, ivar, Rhs, defaultdrop, drop);
    if ( readFlag == FALSE )
    {
        return 0;
    }
    readFlag = spilu_checkDoubleInRange(
            fname, pvApiCtx, ivar, *drop, 0, DBL_MAX);
    if ( readFlag == FALSE )
    {
        return 0;
    }
    return 1;
}

//
// spilu_getArgumentAlpha --
//   Get the alpha parameter.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   rhs (input): the number of input arguments
//   alpha (output) : the alpha value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that drop is positive.
// If drop is not provided or is equal to [], then
// set the default value, alpha=0.5.
//
int spilu_getArgumentAlpha(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        double * alpha)
{
    double defaultalpha;
    int readFlag;

    defaultalpha = 0.5;

    readFlag = spilu_getScalarDouble(
            fname, pvApiCtx, ivar, rhs, defaultalpha, alpha);
    if ( readFlag == FALSE )
    {
        return 0;
    }

    readFlag = spilu_checkDoubleInRange(
            fname, pvApiCtx, ivar, *alpha, 0, DBL_MAX);
    if ( readFlag == FALSE )
    {
        return 0;
    }

    return 1;
}

//
// spilu_getArgumentPermtol --
//   Get the permtol parameter.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   rhs (input): the number of input arguments
//   permtol (output) : the value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that drop is positive.
// If drop is not provided or is equal to [], then
// set the default value, permtol=0.5.
//
int spilu_getArgumentPermtol(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        double * permtol)
{
    double defaultpermtol;
    int readFlag;

    defaultpermtol = 0.5;

    readFlag = spilu_getScalarDouble(
            fname, pvApiCtx, ivar, rhs, defaultpermtol, permtol);
    if ( readFlag == FALSE )
    {
        return 0;
    }

    readFlag = spilu_checkDoubleInRange(
            fname, pvApiCtx, ivar, *permtol, 0, DBL_MAX);
    if ( readFlag == FALSE )
    {
        return 0;
    }
    return 1;
}

//
// spilu_getArgumentBloc --
//   Get the bloc parameter.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   rhs (input): the number of input arguments
//   A (input) : the sparse matrix
//   bloc (output) : the value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that drop is positive.
// If drop is not provided or is equal to [], then
// set the default value, bloc=n.
//
int spilu_getArgumentBloc(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        SciSparse A, int * bloc)
{
    int defaultbloc;
    int readFlag;

    defaultbloc = A.m;

    readFlag = spilu_getScalarIntegerFromScalarDouble(
            fname, pvApiCtx, ivar, rhs, defaultbloc, bloc);
    if ( readFlag == FALSE )
    {
        return 0;
    }

    readFlag = spilu_checkIntegerInRange(
            fname, pvApiCtx, ivar, *bloc, 0, A.m);
    if ( readFlag == FALSE )
    {
        return 0;
    }

    return 1;
}

//
// spilu_getArgumentLfil --
//   Get the lfil parameter.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   rhs (input): the number of input arguments
//   A (input) : the sparse matrix
//   lfil (output) : the value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that drop is positive.
// If drop is not provided or is equal to [], then
// set the default value, lfil = 1 + A.nel/A.n.
//
int spilu_getArgumentLfil(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        SciSparse A, int * lfil)
{
    int defaultlfil;
    int readFlag;

    defaultlfil = 1+((int) (A.nel/A.n));

    readFlag= spilu_getScalarIntegerFromScalarDouble(
            fname, pvApiCtx, ivar, Rhs, defaultlfil, lfil);
    if ( readFlag == FALSE )
    {
        return 0;
    }

    readFlag = spilu_checkIntegerInRange(
            fname, pvApiCtx, ivar, *lfil, 0, A.m);
    if ( readFlag == FALSE)
    {
        return 0;
    }

    return 1;
}

// spilu_allocLUArrays --
//   Allocate arrays of the LU decomposition
//   necessary for the Fortran library.
//
// Arguments
// Input:
// fname : the name of the function
// nrows : the number of rows in the sparse matrix A
// nnzLU : an integer, the estimated number of nonzeros in the LU decomposition
//
// Output:
// ju: a n-by-1 matrix of doubles, pointer to the diagonal elements in alu, jlu
// alu: a (nnzLU+1)-by-1 matrix of doubles, the values of the ALU factors
// jlu: a (nnzLU+1)-by-1 matrix of doubles, pointers to the alu array
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
int spilu_allocLUArrays(
        char * fname, void * pvApiCtx,
        int nnzLU, int nrows,
        double ** alu, int ** jlu, int ** ju)
{
    *alu = (double *)malloc((nnzLU+1)*sizeof(double));
    if (*alu == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        return 0;
    }
    *jlu = (int *)malloc((nnzLU+1)*sizeof(int));
    if (*jlu == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        return 0;
    }
    *ju = (int *)malloc(nrows*sizeof(int));
    if (*ju == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        return 0;
    }
    return 1;
}
// spilu_allocRowptr --
//   Allocate and compute the array ia.
//
// Arguments
// Input:
// fname : the name of the function
// A : a n-by-n sparse matrix
//
// Output:
// ia[0:nrows] (output) : an array with A.m+1 entries, pointers to the rows of A.
//            For i=1, 2,..., A.m, the value ia(i) is the first
//            index of a for the row A(i,:).
//            In other words, for i=1, 2,..., A.m,
//            a(ia(i):ia(i+1)-1) contains the nonzeros of the i-th row.
//            The number of nonzeros is : ia(A.m+1)-1
//            This is the "row_ptr" array in the CSR format.
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
int spilu_allocRowptr(
        char * fname, void * pvApiCtx,
        SciSparse A, int ** ia)
{
    int nrows = 0;
    nrows = A.m;

    *ia = (int *)malloc((nrows+1)*sizeof(int));
    if (*ia == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        return 0;
    }
    spiluc_Sci2spk(A, *ia);
    return 1;
}
