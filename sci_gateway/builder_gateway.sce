// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function buildGw()

    sci_gateway_dir = get_absolute_file_path('builder_gateway.sce');

    lang='c';
    tbx_builder_gateway_lang(lang, sci_gateway_dir);
    languages = [lang];
    tbx_build_gateway_loader(languages, sci_gateway_dir);
    tbx_build_gateway_clean(languages, sci_gateway_dir);

endfunction

buildGw();
clear buildGw;
