Scilab Spilu Toolbox

Purpose
-------

Spilu is a Scilab toolbox which provides preconditioners based on Incomplete LU (ILU) factorizations.
This module is based on a set of Fortran routines from the Sparskit 
module by Yousef Saaf. 
More specifically, this module provides some of the preconditioners 
from the ITSOL sub-module of Sparskit. 

The preconditioners which are provided in this toolbox may be used
in preconditioned iterative algorithms for solving sparse linear systems
of equations.
According to Y. Saad, "roughly speaking, a preconditioner is any form of implicit or explicit modification of
an original linear system which makes it easier to solve by a given iterative method."
Examples of preconditioned iterative algorithms are the Generalized Minimum Residual Method (GMRES)
or the Preconditioned Conjugate Gradient (PCG).
Hence, the Spilu toolbox is the companion of the Imsls toolbox, which
provides these iterative methods.

Features
--------

 * spilu_ilut: Incomplete LU factorization with dual Truncation strategy
 * spilu_ilutp: ilut with column Pivoting
 * spilu_ilud: ILU with single dropping and diagonal compensation
 * spilu_iludp: ILUD with column Pivoting
 * spilu_iluk: level-k ILU
 * spilu_ilu0: simple ILU(0) preconditioning
 * spilu_milu0: MILU(0) preconditioning
 
Support

 * spilu_getpath  Returns the path to the current module.
 * spilu_ilu0M  ILU(0) preconditioning (macro).
 * spilu_ilukM  ILU preconditioning with level of fill-in of k (macro).
 * spilu_permVecToMat  Convert a permutation vector into matrix.

Benchmark

 * spilu_iluhub  A generic hub for various incomplete LU algorithms.
 * spilu_iluhubavail  Returns the available algorithms.
 * spilu_iluhubparname  Returns the name of a ILU parameter, given the index.
 
Graphics

 * spilu_iludplot  Plots the sensitivity of ILUD for A.
 * spilu_iludpplot  Plots the sensitivity of ILUDP for A.
 * spilu_ilukplot  Plots the sensitivity of ILUK for A.
 * spilu_ilutplot  Plots the sensitivity of ILUT for A.
 * spilu_ilutpplot  Plots the sensitivity of ILUT for A.
 * spilu_plotparameter  Plots the sensitivity of a decomposition algorithm.
 
Dependencies
------------

 * This module depends on the MatrixMarket module.
 * This module depends on the assert module.
 * This module depends on the helptbx module (to build the help pages).
 * This module depends on the imsls module (the imsls_spdiags function).
 * This module depends on the apifun module.

History
-------

The Sparskit toolkit was developped in 1996 by Y.Saad,
University of Minnesota. 

In 2005, this module was developed by Aladin Group (IRISA-INRIA) in order 
to provide a Scilab connection to the Sparskit ILU decomposition functions. 

In 2010 - 2011, Michael Baudin (Digitιo) and Benoit Goepfert (NII internship) 
updated this module for Scilab 5. 
Additionnally, bugs were fixed, the gateways were updated, 
help pages were updated and unit test were created. 
Finally, new functions were created to help the use of the toolbox.

TODO
----

 * 


Licence
-------

This toolbox is released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Authors
-------

 * Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
 * Copyright (C) 2000 - 2001 - INRIA - Aladin Group
 * Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2011 - NII - Benoit Goepfert

Bibliography
------------

 * http://www.irisa.fr/aladin/codes/SCILIN/
 * http://www.netlib.org/templates/
 * http://graal.ens-lyon.fr/~jylexcel/scilab-sparse/meeting07/
 * http://www-users.cs.umn.edu/~saad/software/SPARSKIT/index.html
 * "SPARSKIT: A basic tool-kit for sparse matrix computations", Youcef Saad, 1994
 * http://people.sc.fsu.edu/~jburkardt/f77_src/sparsekit/sparsekit.html

