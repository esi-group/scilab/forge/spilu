// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// This script is a Scilab port of SPARSKIT2/MATGEN/FDIF/genmat.f
// The algorithm has many non-vectorizable loops, which may be unefficient 
// in the Scilab language. 
// This is for testing purposes only.

//----------------------------------------------------------------------c
//                          S P A R S K I T                             c
//----------------------------------------------------------------------c
//    MATRIX GENERATION ROUTINES  -- FINITE DIFFERENCE MATRICES         c
//----------------------------------------------------------------------c
// contents:                                                            c
//----------                                                            c
// gen57pt  : generates 5-point and 7-point matrices.                   c
// gen57bl  : generates block 5-point and 7-point matrices.             c
//                                                                      c
// supporting routines:                                                 c
//---------                                                             c
// gensten  : generate the stencil (point version)                      c
// bsten    : generate the stencil (block version)                      c
// fdaddbc  : finite difference add boundary conditions                 c
// fdreduce : reduce the system to eliminate node with known values     c
// clrow    : clear a row of a CSR matrix                               c
// lctcsr   : locate the position of A(i,j) in CSR format               c
//----------------------------------------------------------------------c
function [n,a,ja,ia,iau,rhs] = gen57pt(nx,ny,nz,al,mmode)

    //-----------------------------------------------------------------------
    // On entry:
    //
    // nx      = number of grid points in x direction
    // ny	  = number of grid points in y direction
    // nz	  = number of grid points in z direction
    // al      = array of size 6, carries the coefficient alpha of the
    //           boundary conditions
    // mmode    = what to generate:
    //           < 0 : generate the graph only,
    //           = 0 : generate the matrix,
    //           > 0 : generate the matrix and the right-hand side.
    //
    // On exit:
    //
    // n       = number of nodes with unknown values, ie number of rows
    //           in the matrix
    //
    // a,ja,ia = resulting matrix in row-sparse format
    //
    // iau     = integer*n, containing the poisition of the diagonal element
    //           in the a, ja, ia structure
    //
    // rhs     = the right-hand side
    //
    // External functions needed (must be supplied by caller)
    //     afun, bfun, cfun, dfun, efun, ffun, gfun, hfun
    //     betfun, gamfun
    // They have the following prototype:
    //     real*8 function xfun(x, y, z)
    //     real*8 x, y, z
    //-----------------------------------------------------------------------
    // This function computes the sparse matrix in compressed sparse row
    // format for the elliptic equation:
    //       d    du    d    du    d    du      du     du     du
    // L u = --(A --) + --(B --) + --(C --) + D -- + E -- + F -- + G u = H u
    //       dx   dx    dy   dy    dz   dz      dx     dy     dz
    //
    // with general Mixed Boundary conditions, on a rectangular 1-D,
    // 2-D or 3-D grid using 2nd order centered difference schemes.
    //
    // The functions a, b, ..., g, h are known through the
    // as afun, bfun, ..., gfun, hfun in this function.
    // NOTE: To obtain the correct matrix, any function that is not
    // needed should be set to 0. .  For example for 2.-dimensional
    // problems, nz should be set to 1 and the functions cfun and ffun
    // should be 0.  functions.
    //
    // The Boundary condition is specified in the following form:
    //           du
    //     alpha -- + beta u = gamma
    //           dn
    // Where alpha is constant at each side of the boundary surfaces.  Alpha
    // is represented by parameter al.  It is expected to an array that
    // contains enough elements to specify the boundaries for the problem,
    // 1-D case needs 2. elements, 2-D needs 4 and 3-D needs 6.  The order
    // of the boundaries in the array is left(west), right(east),
    // bottom(south), top(north), front, rear.  Beta and gamma are functions
    // of type real with three arguments x, y, z.  These 2. functions are
    // known function 'addbc' as betfun and gamfun.  They should following
    // the same notion as afun ... hfun.  For more restriction on afun ...
    // hfun, please read the documentation follows the function 'getsten',
    // and, for more on betfun and gamfun, please refer to the documentation
    // under function 'fdaddbc'.
    //
    // The nodes are ordered using natural ordering, first x direction, then
    // y, then z.  The mesh size h is uniform and determined by grid points
    // in the x-direction.
    //
    // The domain specified for the problem is [0 .ge. x .ge. 1],
    // [0 .ge. y .ge. (ny-1)*h] and [0 .ge. z .ge. (nz-1)*h], where h is
    // 1 / (nx-1).  Thus if non-Dirichlet boundary condition is specified,
    // the mesh will have nx points along the x direction, ny along y and
    // nz along z.  For 1-D case, both y and z value are assumed to 0. 
    // when calling relavent functions that have three parameters.
    // Similarly, for 2-D case, z is assumed to be 0. .
    //
    // About the expectation of nx, ny and nz:
    // nx is required to be > 1 always;
    // if the second dimension is present in the problem, then ny should be
    // > 1, else 1;
    // if the third dimension is present in the problem, nz > 1, else 1.
    // when ny is 1, nz must be 1.
    //-----------------------------------------------------------------------
    //
    //     stencil [1:7] has the following meaning:
    //
    //     center point = stencil(1)
    //     west point = stencil(2)
    //     east point = stencil(3)
    //     south point = stencil(4)
    //     north point = stencil(5)
    //     front point = stencil(6)
    //     back point = stencil(7)
    //
    //     al[1:6] carry the coeffsicient alpha in the similar order
    //
    //     west  side = al(1)
    //     east  side = al(2)
    //     south side = al(3)
    //     north side = al(4)
    //     front side = al(5)
    //     back  side = al(6)
    //
    //                           al(4)
    //                           st(5)
    //                            |
    //                            |
    //                            |           al(6)
    //                            |          .st(7)
    //                            |     .
    //         al(1)              | .             al(2)
    //         st(2) ----------- st(1) ---------- st(3)
    //                       .    |
    //                   .        |
    //               .            |
    //            st(6)           |
    //            al(5)           |
    //                            |
    //                           st(4)
    //                           al(3)
    //
    //
    //     local variables
    //
    //
    //     nx has to be larger than 1
    //
    rhs = zeros(nx*ny*nz,1)
    if (nx<=1) then
        return
    end
    h = 1. / (nx-1)
    //
    //     first generate the whole matrix as if the boundary condition does
    //     not exist
    //
    kx = 1
    ky = nx
    kz = nx*ny
    iedge = 1
    node = 1
    for iz = 1:nz
        for iy = 1:ny
            for ix = 1:nx
                ia(node) = iedge;
                //
                //     compute the stencil at the current node
                //
                if (mmode>=0) then
                    [stencil,r] = getsten(nx,ny,nz,mmode,ix-1,iy-1,iz-1,h);
                end
                //     west
                if (ix>1) then
                    ja(iedge)=node-kx;
                    if (mmode>=0) then
                        a(iedge) = stencil(2);
                    end
                    iedge=iedge + 1;
                end
                //     south
                if (iy>1) then
                    ja(iedge)=node-ky;
                    if (mmode>=0) then
                        a(iedge) = stencil(4);
                    end
                    iedge=iedge + 1;
                end
                //     front plane
                if (iz>1) then
                    ja(iedge)=node-kz;
                    if (mmode>=0) then
                        a(iedge) = stencil(6);
                    end
                    iedge=iedge + 1;
                end
                //     center node
                ja(iedge) = node;
                iau(node) = iedge;
                if (mmode>=0) then
                    a(iedge) = stencil(1);
                end
                iedge = iedge + 1;
                //     east
                if (ix<nx) then
                    ja(iedge)=node+kx;
                    if (mmode>=0) then
                        a(iedge) = stencil(3);
                    end
                    iedge=iedge + 1;
                end
                //     north
                if (iy<ny) then
                    ja(iedge)=node+ky;
                    if (mmode>=0) then 
                        a(iedge) = stencil(5);
                    end
                    iedge=iedge + 1;
                end
                //     back plane
                if (iz<nz) then
                    ja(iedge)=node+kz;
                    if (mmode>=0) then 
                        a(iedge) = stencil(7);
                    end
                    iedge=iedge + 1;
                end
                //     the right-hand side
                if (mmode>0) then
                    rhs(node) = r;
                end
                node=node+1;
            end
        end
    end
    ia(node)=iedge;
    //
    //     Add in the boundary conditions
    //
    [a,rhs] = fdaddbc(nx,ny,nz,a,ja,ia,iau,rhs,al,h);
    //
    //     eliminate the boudary nodes from the matrix
    //
    [n,a,ja,ia,iau,rhs,stencil] = fdreduce(nx,ny,nz,al,a,ja,ia,iau,rhs,stencil);
endfunction

function [stencil,rhs] = getsten (nx,ny,nz,mmode,kx,ky,kz,h)

    //-----------------------------------------------------------------------
    //     This function calculates the correct stencil values for
    //     centered difference discretization of the elliptic operator
    //     and the right-hand side
    //
    // L u = delx( A delx u ) + dely ( B dely u) + delz ( C delz u ) +
    //	delx ( D u ) + dely (E u) + delz( F u ) + G u = H
    //
    //   For 2-D problems the discretization formula that is used is:
    //
    // h**2 * Lu == A(i+1/2,j)*{u(i+1,j) - u(i,j)} +
    //	       A(i-1/2,j)*{u(i-1,j) - u(i,j)} +
    //              B(i,j+1/2)*{u(i,j+1) - u(i,j)} +
    //              B(i,j-1/2)*{u(i,j-1) - u(i,j)} +
    //              (h/2)*D(i,j)*{u(i+1,j) - u(i-1,j)} +
    //              (h/2)*E(i,j)*{u(i,j+1) - u(i,j-1)} +
    //              (h/2)*E(i,j)*{u(i,j+1) - u(i,j-1)} +
    //              (h**2)*G(i,j)*u(i,j)
    //-----------------------------------------------------------------------

    stencil = []
    rhs = []
    //
    //     if mmode < 0, we shouldn't have come here
    //
    if (mmode < 0) then
        return
    end
    //
    for k=1:7
        stencil(k) = 0. 
    end
    //
    hhalf = h*0.5
    x = h*kx
    y = h*ky
    z = h*kz
    cntr = 0. 
    //     differentiation wrt x:
    coeffs = afun(x+hhalf,y,z)
    stencil(3) = stencil(3) + coeffs
    cntr = cntr + coeffs
    //
    coeffs = afun(x-hhalf,y,z)
    stencil(2) = stencil(2) + coeffs
    cntr = cntr + coeffs
    //
    coeffs = dfun(x,y,z)*hhalf
    stencil(3) = stencil(3) + coeffs
    stencil(2) = stencil(2) - coeffs
    if (ny > 1) then
        //
        //     differentiation wrt y:
        //
        coeffs = bfun(x,y+hhalf,z)
        stencil(5) = stencil(5) + coeffs
        cntr = cntr + coeffs
        //
        coeffs = bfun(x,y-hhalf,z)
        stencil(4) = stencil(4) + coeffs
        cntr = cntr + coeffs
        //
        coeffs = efun(x,y,z)*hhalf
        stencil(5) = stencil(5) + coeffs
        stencil(4) = stencil(4) - coeffs
    end
    if (nz > 1) then
        //
        // differentiation wrt z:
        //
        coeffs = cfun(x,y,z+hhalf)
        stencil(7) = stencil(7) + coeffs
        cntr = cntr + coeffs
        //
        coeffs = cfun(x,y,z-hhalf)
        stencil(6) = stencil(6) + coeffs
        cntr = cntr + coeffs
        //
        coeffs = ffun(x,y,z)*hhalf
        stencil(7) = stencil(7) + coeffs
        stencil(6) = stencil(6) - coeffs
    end
    //
    // contribution from function G:
    //
    coeffs = gfun(x,y,z)
    stencil(1) = h*h*coeffs - cntr
    //
    //     the right-hand side
    //
    if (mmode > 0) then
        rhs = h*h*hfun(x,y,z)
    end
    //
endfunction

function [n,a,ja,ia,iau] = gen57bl (nx,ny,nz,nfree,na,stencil)

    //--------------------------------------------------------------------
    // This function computes the sparse matrix in compressed
    // format for the elliptic operator
    //
    // L u = delx( a . delx u ) + dely ( b . dely u) + delz ( c . delz u ) +
    //	delx ( d . u ) + dely (e . u) + delz( f . u ) + g . u
    //
    // Here u is a vector of nfree componebts and each of the functions
    // a, b, c, d, e, f, g   is an (nfree x nfree) matrix depending of
    // the coordinate (x,y,z).
    // with Dirichlet Boundary conditions, on a rectangular 1-D,
    // 2-D or 3-D grid using centered difference schemes.
    //
    // The functions a, b, ..., g are known through the
    // subroutines  afunbl, bfunbl, ..., gfunbl. (user supplied) .
    //
    // uses natural ordering, first x direction, then y, then z
    // mesh size h is uniform and determined by grid points
    // in the x-direction.
    // 
    // The output matrix is in Block -- Sparse Row format. 
    //
    //--------------------------------------------------------------------
    // parameters:
    //-------------
    // Input:
    // ------
    // nx      = number of points in x direction
    // ny	  = number of points in y direction
    // nz	  = number of points in z direction
    // nfree   = number of degrees of freedom per point
    // na	  = first dimension of array a as declared in calling
    //           program. Must be .ge. nfree**2
    //
    // Output: 
    // ------ 
    // n	  = dimension of matrix (output)
    //
    // a, ja, ia = resulting matrix in  Block Sparse Row format
    //           a(1:nfree**2, j ) contains a nonzero block and ja(j) 
    //           contains the (block) column number of this block.
    //           the block dimension of the matrix is n (output) and 
    //           therefore the total number of (scalar) rows is n x nfree.
    //     
    // iau     = integer*n containing the position of the diagonal element
    //           in the a, ja, ia structure
    //
    // Work space:
    //------------ 
    // stencil =  work array of size (7,nfree**2) [stores local stencils]
    //
    //--------------------------------------------------------------------
    //
    //     stencil (1:7,*) has the following meaning:
    //
    //     center point = stencil(1)
    //     west point   = stencil(2)
    //     east point   = stencil(3)
    //     south point  = stencil(4)
    //     north point  = stencil(5)
    //     front point  = stencil(6)
    //     back point   = stencil(7)
    //
    //
    //                           st(5)
    //                            |
    //                            |
    //                            |
    //                            |          .st(7)
    //                            |     .
    //                            | .
    //         st(2) ----------- st(1) ---------- st(3)
    //                       .    |
    //                   .        |
    //               .            |
    //            st(6)           |
    //                            |
    //                            |
    //                           st(4)
    //
    //
    h = 1./dble(nx+1)
    kx = 1
    ky = nx
    kz = nx*ny
    nfree2 = nfree*nfree
    iedge = 1
    node = 1
    for iz=1:nz
        for iy = 1:ny
            for ix = 1:nx
                ia(node) = iedge
                stencil = bsten(nx,ny,nz,ix,iy,iz,nfree,h)
                //     west
                if (ix>1) then
                    ja(iedge)=node-kx
                    for k=1:nfree2
                        a(k,iedge) = stencil(2,k)
                    end
                    iedge=iedge + 1
                end
                //     south
                if (iy>1) then
                    ja(iedge)=node-ky
                    for k=1:nfree2
                        a(k,iedge) = stencil(4,k)
                    end
                    iedge=iedge + 1
                end
                //     front plane
                if (iz>1) then
                    ja(iedge)=node-kz
                    for k=1:nfree2
                        a(k,iedge) = stencil(6,k)
                    end
                    iedge=iedge + 1
                end
                //     center node
                ja(iedge) = node
                iau(node) = iedge
                for k=1:nfree2
                    a(k,iedge) = stencil(1,k)
                end
                iedge = iedge + 1
                //     -- upper part
                //     east
                if (ix<nx) then
                    ja(iedge)=node+kx
                    for k=1:nfree2
                        a(k,iedge) = stencil(3,k)
                    end
                    iedge=iedge + 1
                end
                //     north
                if (iy<ny) then
                    ja(iedge)=node+ky
                    for k=1:nfree2
                        a(k,iedge) = stencil(5,k)
                    end
                    iedge=iedge + 1
                end
                //     back plane
                if (iz<nz) then
                    ja(iedge)=node+kz
                    for k=1:nfree2
                        a(k,iedge) = stencil(7,k)
                    end
                    iedge=iedge + 1
                end
                //------next node -------------------------
                node=node+1
            end
        end
    end
    //     
    //     -- new version of BSR -- renumbering removed. 
    //     change numbering of nodes so that each ja(k) will contain the
    //     actual column number in the original matrix of entry (1,1) of each
    //     block (k).
    //      do 101 k=1:iedge-1
    //         ja(k) = (ja(k)-1)*nfree+1
    // 101  continue
    //
    //      n = (node-1)*nfree
    n = node-1 
    ia(node)=iedge
endfunction

function stencil = bsten (nx,ny,nz,kx,ky,kz,nfree,h)
    //-----------------------------------------------------------------------
    //     This function calcultes the correct block-stencil values for
    //     centered difference discretization of the elliptic operator
    //     (block version of stencil)
    //
    // L u = delx( a delx u ) + dely ( b dely u) + delz ( c delz u ) +
    //       d delx ( u ) + e dely (u) + f delz( u ) + g u
    //
    //   For 2-D problems the discretization formula that is used is:
    //
    // h**2 * Lu == a(i+1/2,j)*{u(i+1,j) - u(i,j)} +
    //	       a(i-1/2,j)*{u(i-1,j) - u(i,j)} +
    //              b(i,j+1/2)*{u(i,j+1) - u(i,j)} +
    //              b(i,j-1/2)*{u(i,j-1) - u(i,j)} +
    //              (h/2)*d(i,j)*{u(i+1,j) - u(i-1,j)} +
    //              (h/2)*e(i,j)*{u(i,j+1) - u(i,j-1)} +
    //              (h/2)*e(i,j)*{u(i,j+1) - u(i,j-1)} +
    //              (h**2)*g(i,j)*u(i,j)
    //-----------------------------------------------------------------------
    //------------
    if (nfree > 15) then
        error(' ERROR ** nfree too large ')
    end
    //
    nfree2 = nfree*nfree
    for k=1: nfree2
        cntr(k) = 0. 
        for i=1:7
            stencil(i,k) = 0. 
        end
    end
    //------------
    hhalf = h*0.5
    h2 = h*h
    x = h*kx
    y = h*ky
    z = h*kz
    // differentiation wrt x:
    coeffs = afunbl(nfree,x+hhalf,y,z)
    for k=1: nfree2
        stencil(3,k) = stencil(3,k) + coeffs(k)
        cntr(k) = cntr(k) + coeffs(k)
    end
    //
    coeffs = afunbl(nfree,x-hhalf,y,z)
    for k=1: nfree2
        stencil(2,k) = stencil(2,k) + coeffs(k)
        cntr(k) = cntr(k) + coeffs(k)
    end
    //
    coeffs = dfunbl(nfree,x,y,z)
    for k=1: nfree2
        stencil(3,k) = stencil(3,k) + coeffs(k)*hhalf
        stencil(2,k) = stencil(2,k) - coeffs(k)*hhalf
    end
    if (ny > 1) then
        //
        // differentiation wrt y:
        //
        coeffs = bfunbl(nfree,x,y+hhalf,z)
        for k=1:nfree2
            stencil(5,k) = stencil(5,k) + coeffs(k)
            cntr(k) = cntr(k) + coeffs(k)
        end
        //
        coeffs = bfunbl(nfree,x,y-hhalf,z)
        for k=1: nfree2
            stencil(4,k) = stencil(4,k) + coeffs(k)
            cntr(k) = cntr(k) + coeffs(k)
        end
        //
        coeffs = efunbl(nfree,x,y,z)
        for k=1: nfree2
            stencil(5,k) = stencil(5,k) + coeffs(k)*hhalf
            stencil(4,k) = stencil(4,k) - coeffs(k)*hhalf
        end
    end
    if (nz > 1) then
        //
        // differentiation wrt z:
        //
        coeffs = cfunbl(nfree,x,y,z+hhalf)
        for k=1: nfree2
            stencil(7,k) = stencil(7,k) + coeffs(k)
            cntr(k) = cntr(k) + coeffs(k)
        end
        //
        coeffs = cfunbl(nfree,x,y,z-hhalf)
        for k=1: nfree2
            stencil(6,k) = stencil(6,k) + coeffs(k)
            cntr(k) = cntr(k) + coeffs(k)
        end
        //
        coeffs = ffunbl(nfree,x,y,z)
        for k=1: nfree2
            stencil(7,k) = stencil(7,k) + coeffs(k)*hhalf
            stencil(6,k) = stencil(6,k) - coeffs(k)*hhalf
        end
    end
    //
    // discretization of  product by g:
    //
    coeffs = gfunbl(nfree,x,y,z)
    for k=1: nfree2
        stencil(1,k) = h2*coeffs(k) - cntr(k)
    end
    //
endfunction

function [n,a,ja,ia,iau,rhs,stencil] = fdreduce(nx,ny,nz,alpha,a,ja,ia,iau,rhs,stencil)

    //-----------------------------------------------------------------------
    // This function tries to reduce the size of the matrix by looking
    // for Dirichlet boundary conditions at each surface and solve the boundary
    // value and modify the right-hand side of related nodes, then clapse all
    // the boundary nodes.
    //-----------------------------------------------------------------------

    //
    //     The first 0.5 of this function will try to change the right-hand
    //     side of all the nodes that has a neighbor with Dirichlet boundary
    //     condition, since in this case the value of the boundary point is
    //     known.
    //     Then in the second 0.5, we will try to eliminate the boundary
    //     points with known values (with Dirichlet boundary condition).
    //
    kx = 1
    ky = nx
    kz = nx*ny
    lx = 1
    ux = nx
    ly = 1
    uy = ny
    lz = 1
    uz = nz
    //
    //     Here goes the first part. ----------------------------------------
    //
    //     the left (west) side
    //
    if (alpha(1) == 0. ) then
        lx = 2
        for k=1: nz
            for j=1: ny
                node = (k-1)*kz + (j-1)*ky + 1;
                nbnode = node + kx;
                lk = lctcsr(nbnode, node, ja, ia);
                ld = iau(node);
                val = rhs(node)/a(ld);
                //     modify the rhs
                rhs(nbnode) = rhs(nbnode) - a(lk)*val;
            end
        end
    end
    //
    //     right (east) side
    //
    if (alpha(2) == 0. ) then
        ux = nx - 1;
        for k=1: nz
            for j=1: ny
                node = (k-1)*kz + (j-1)*ky + nx;
                nbnode = node - kx;
                lk = lctcsr(nbnode, node, ja, ia);
                ld = iau(node);
                val = rhs(node)/a(ld);
                //     modify the rhs
                rhs(nbnode) = rhs(nbnode) - a(lk)*val;
            end
        end
    end
    //
    //     if it's only 1-D, skip the following part
    //
    if (ny > 1) then
        //
        //     the bottom (south) side
        //
        if (alpha(3) == 0. ) then
            ly = 2;
            for k=1: nz
                for i = lx: ux
                    node = (k-1)*kz + i;
                    nbnode = node + ky;
                    lk = lctcsr(nbnode, node, ja, ia);
                    ld = iau(node);
                    val = rhs(node)/a(ld);
                    //     modify the rhs
                    rhs(nbnode) = rhs(nbnode) - a(lk)*val;
                end
            end
        end
        //
        //     top (north) side
        //
        if (alpha(4) == 0. ) then
            uy = ny - 1
            for k=1: nz
                for i = lx: ux
                    node = (k-1)*kz + i + (ny-1)*ky
                    nbnode = node - ky
                    lk = lctcsr(nbnode, node, ja, ia)
                    ld = iau(node)
                    val = rhs(node)/a(ld)
                    //     modify the rhs
                    rhs(nbnode) = rhs(nbnode) - a(lk)*val
                end
            end
        end
        //
        //     if only 2-D skip the following section on z
        //
        if (nz > 1) then
            //
            //     the front surface
            //
            if (alpha(5) == 0. ) then
                lz = 2
                for j = ly: uy
                    for i = lx:  ux
                        node = (j-1)*ky + i
                        nbnode = node + kz
                        lk = lctcsr(nbnode, node, ja, ia)
                        ld = iau(node)
                        val = rhs(node)/a(ld)
                        //     modify the rhs
                        rhs(nbnode) = rhs(nbnode) - a(lk)*val
                    end
                end
            end
            //
            //     rear surface
            //
            if (alpha(6) == 0. ) then
                uz = nz - 1
                for j = ly: uy
                    for i = lx: ux
                        node = (nz-1)*kz + (j-1)*ky + i
                        nbnode = node - kz
                        lk = lctcsr(nbnode, node, ja, ia)
                        ld = iau(node)
                        val = rhs(node)/a(ld)
                        //     modify the rhs
                        rhs(nbnode) = rhs(nbnode) - a(lk)*val
                    end
                end
            end
        end
    end
    //
    //     now the second part ----------------------------------------------
    //
    //     go through all the actual nodes with unknown values, collect all
    //     of them to form a new matrix in compressed sparse row format.
    //
    kx = 1
    ky = ux - lx + 1
    kz = (uy - ly + 1) * ky
    node = 1
    iedge = 1
    for k = lz: uz
        for j = ly: uy
            for i = lx: ux
                //
                //     the corresponding old node number
                nbnode = ((k-1)*ny + j-1)*nx + i
                //
                //     copy the row into local stencil, copy is done is the exact
                //     same order as the stencil is written into array a
                lk = ia(nbnode)
                if (i>1) then
                    stencil(2) = a(lk)
                    lk = lk + 1
                end
                if (j>1) then
                    stencil(4) = a(lk)
                    lk = lk + 1
                end
                if (k>1) then
                    stencil(6) = a(lk)
                    lk = lk + 1
                end
                stencil(1) = a(lk)
                lk = lk + 1
                if (i<nx) then
                    stencil(3) = a(lk)
                    lk = lk + 1
                end
                if (j<ny) then
                    stencil(5) = a(lk)
                    lk = lk + 1
                end
                if (k<nz) then
                    stencil(7) = a(lk)
                end
                //
                //     first the ia pointer -- points to the beginning of each row
                ia(node) = iedge
                //
                //     move the values from the local stencil to the new matrix
                //
                //     the neighbor on the left (west)
                if (i>lx) then
                    ja(iedge)=node-kx
                    a(iedge) =stencil(2)
                    iedge=iedge + 1
                end
                //     the neighbor below (south)
                if (j>ly) then
                    ja(iedge)=node-ky
                    a(iedge)=stencil(4)
                    iedge=iedge + 1
                end
                //     the neighbor in the front
                if (k>lz) then
                    ja(iedge)=node-kz
                    a(iedge)=stencil(6)
                    iedge=iedge + 1
                end
                //     center node (itself)
                ja(iedge) = node
                iau(node) = iedge
                a(iedge) = stencil(1)
                iedge = iedge + 1
                //     the neighbor to the right (east)
                if (i<ux) then
                    ja(iedge)=node+kx
                    a(iedge)=stencil(3)
                    iedge=iedge + 1
                end
                //     the neighbor above (north)
                if (j<uy) then
                    ja(iedge)=node+ky
                    a(iedge)=stencil(5)
                    iedge=iedge + 1
                end
                //     the neighbor at the back
                if (k<uz) then
                    ja(iedge)=node+kz
                    a(iedge)=stencil(7)
                    iedge=iedge + 1
                end
                //     the right-hand side
                rhs(node) = rhs(nbnode)
                //------next node -------------------------
                node=node+1
                //
            end
        end
    end
    //
    ia(node) = iedge
    //
    //     the number of nodes in the final matrix is stored in n
    //
    n = node - 1
endfunction

function [a,rhs] = fdaddbc(nx,ny,nz,a,ja,ia,iau,rhs,al,h)

    //-----------------------------------------------------------------------
    // This function will add the boundary condition to the linear system
    // consutructed without considering the boundary conditions
    //
    // The Boundary condition is specified in the following form:
    //           du
    //     alpha -- + beta u = gamma
    //           dn
    // Alpha is stored in array AL.  The six side of the boundary appares
    // in AL in the following order: left(west), right(east), bottom(south),
    // top(north), front, back(rear). (see also the illustration in gen57pt)
    // Beta and gamma appears as the functions, betfun and gamfun.
    // They have the following prototype
    //
    // real*8 function xxxfun(x, y, z)
    // real*8 x, y, z
    //
    // where x, y, z are vales in the range of [0, 1][0, (ny-1)*h]
    // [0, (nz-1)*h]
    //
    // At the corners or boundary lines, the boundary conditions are applied
    // in the follow order:
    // 1) if 1. side is Dirichlet boundary condition, the Dirichlet boundary
    //    condition is used;
    // 2) if more than 1. sides are Dirichlet, the Direichlet condition
    //    specified for X direction boundary will overwrite the 1. specified
    //    for Y direction boundary which in turn has priority over Z
    //     direction boundaries.
    // 3) when all sides are non-Dirichlet, the average values are used.
    //-----------------------------------------------------------------------
    //
    
    hhalf = 0.5 * h
    kx = 1
    ky = nx
    kz = nx*ny
    //
    //     In 3-D case, we need to go through all 6 faces 1. by 1.. If
    //     the actual dimension is lower, test on ny is performed first.
    //     If ny is less or equals to 1, then the value of nz is not
    //     checked.
    //-----
    //     the surface on the left (west) side
    //     Concentrate on the contribution from the derivatives related to x,
    //     The terms with derivative of x was assumed to be:
    //
    //     a(3/2,j,k)*[u(2,j,k)-u(1,j,k)] + a(1/2,j,k)*[u(0,j,k)-u(1,j,k)] +
    //     h*d(1,j,k)*[u(2,j,k)-u(0,j,k)]/2
    //
    //     But they actually are:
    //
    //     2*{a(3/2,j,k)*[u(2,j,k)-u(1,j,k)] -
    //     h*a(1,j,k)*[beta*u(1,j,k)-gamma]/alpha]} +
    //     h*h*d(1,j,k)*[beta*u(1,j,k)-gamma]/alpha
    //
    //     Therefore, in terms of local stencil the right neighbor of a node
    //     should be changed to 2*a(3/2,j,k),
    //     The matrix never contains the left neighbor on this border, nothing
    //     needs to be done about it.
    //     The following terms should be added to the center stencil:
    //     -a(3/2,j,k) + a(1/2,j,k) + [h*d(1,j,k)-2*a(1,j,k)]*h*beta/alpha
    //
    //     And these terms should be added to the corresponding right-hand side
    //     [h*d(1,j,k)-2*a(1,j,k)]*h*gamma/alpha
    //
    //     Obviously, the formula do not apply for the Dirichlet Boundary
    //     Condition, where alpha will be 0. . In that case, we simply set
    //     all the elements in the corresponding row to 0. (0), then let
    //     the diagonal element be beta, and the right-hand side be gamma.
    //     Thus the value of u at that point will be set. Later on point
    //     like this will be removed from the matrix, since they are of
    //     know value before solving the system.(not done in this function)
    //
    x = 0. 
    side = 'x1'
    for k=1: nz
        z = (k-1)*h;
        for j=1: ny
            y = (j-1)*h;
            node = 1+(j-1)*ky+(k-1)*kz;
            //
            //     check to see if it's Dirichlet Boundary condition here
            //
            if (al(1) == 0. ) then
                a = clrow(node, a, ia);
                a(iau(node)) = betfun(side,x,y,z);
                rhs(node) = gamfun(side,x,y,z);
            else
                //
                //     compute the terms formulated above to modify the matrix.
                //
                //     the right neighbor is stroed in nbr'th posiiton in the a
                nbr = lctcsr(node, node+kx, ja, ia);
                //
                coeffs = 2.*afun(x,y,z);
                ctr = (h*dfun(x,y,z) - coeffs)*h/al(1);
                rhs(node) = rhs(node) + ctr * gamfun(side,x,y,z);
                ctr = afun(x-hhalf,y,z) + ctr * betfun(side,x,y,z);
                coeffs = afun(x+hhalf,y,z);
                a(iau(node)) = a(iau(node)) - coeffs + ctr;
                a(nbr) = 2.*coeffs;
            end
        end
    end
    //
    //     the right (east) side boudary, similarly, the contirbution from
    //     the terms containing the derivatives of x were assumed to be
    //
    //     a(nx+1/2,j,k)*[u(nx+1,j,k)-u(nx,j,k)] +
    //     a(nx-1/2,j,k)*[u(nx-1,j,k)-u(nx,j,k)] +
    //     d(nx,j,k)*[u(nx+1,j,k)-u(nx-1,j,k)]*h/2
    //
    //     Actualy they are:
    //
    //     2*{h*a(nx,j,k)*[gamma-beta*u(nx,j,k)]/alpha +
    //     a(nx-1/2,j,k)*[u(nx-1,j,k)-u(nx,j,k)]} +
    //     h*h*d(nx,j,k)*[gamma-beta*u(nx,j,k)]/alpha
    //
    //     The left stencil has to be set to 2*a(nx-1/2,j,k)
    //
    //     The following terms have to be added to the center stencil:
    //
    //     -a(nx-1/2,j,k)+a(nx+1/2,j,k)-[2*a(nx,j,k)+h*d(nx,j,k)]*beta/alpha
    //
    //     The following terms have to be added to the right-hand side:
    //
    //     -[2*a(nx,j,k)+h*d(nx,j,k)]*h*gamma/alpha
    //
    x = 1.
    side = 'x2'
    for k=1: nz
        z = (k-1)*h;
        for j=1: ny
            y = (j-1)*h;
            node = (k-1)*kz + j*ky;
            //
            if (al(2) == 0. ) then
                a = clrow(node, a, ia);
                a(iau(node)) = betfun(side,x,y,z);
                rhs(node) = gamfun(side,x,y,z);
            else
                nbr = lctcsr(node, node-kx, ja, ia);
                //
                coeffs = 2.*afun(x,y,z);
                ctr = (coeffs + h*dfun(x,y,z))*h/al(2);
                rhs(node) = rhs(node) - ctr * gamfun(side,x,y,z);
                ctr = afun(x+hhalf,y,z) - ctr * betfun(side,x,y,z);
                coeffs = afun(x-hhalf,y,z);
                a(iau(node)) = a(iau(node)) - coeffs + ctr;
                a(nbr) = 2.*coeffs;
            end
        end
    end
    //
    //     If only 1. dimension, return now
    //
    if (ny <= 1) then
        return
    end
    //
    //     the bottom (south) side suface, This similar to the situation
    //     with the left side, except all the function and realted variation
    //     should be on the y.
    //
    //     These 2. block if statment here is to resolve the possible conflict
    //     of assign the boundary value differently by different side of the
    //     Dirichlet Boundary Conditions. They ensure that the edges that have
    //     be assigned a specific value will not be reassigned.
    //
    if (al(1) == 0. ) then
        lx = 2
    else
        lx = 1
    end
    if (al(2) == 0. ) then
        ux = nx-1
    else
        ux = nx
    end
    y = 0. 
    side = 'y1'
    for k=1: nz
        z = (k-1)*h;
        for i = lx: ux
            x = (i-1)*h;
            node = i + (k-1)*kz;
            //
            if (al(3) == 0. ) then
                a = clrow(node, a, ia);
                a(iau(node)) = betfun(side,x,y,z);
                rhs(node) = gamfun(side,x,y,z);
            else
                nbr = lctcsr(node, node+ky, ja, ia);
                //
                coeffs = 2.*bfun(x,y,z);
                ctr = (h*efun(x,y,z) - coeffs)*h/al(3);
                rhs(node) = rhs(node) + ctr * gamfun(side,x,y,z);
                ctr = bfun(x,y-hhalf,z) + ctr * betfun(side,x,y,z);
                coeffs = bfun(x,y+hhalf,z);
                a(iau(node)) = a(iau(node)) - coeffs + ctr;
                a(nbr) = 2.*coeffs;
            end
        end
    end
    //
    //     The top (north) side, similar to the right side
    //
    y = (ny-1) * h
    side = 'y2'
    for k=1: nz
        z = (k-1)*h;
        for i = lx: ux
            x = (i-1)*h;
            node = (k-1)*kz+(ny-1)*ky + i;
            //
            if (al(4) == 0. ) then
                a = clrow(node, a, ia);
                a(iau(node)) = betfun(side,x,y,z);
                rhs(node) = gamfun(side,x,y,z);
            else
                nbr = lctcsr(node, node-ky, ja, ia);
                //
                coeffs = 2.*bfun(x,y,z);
                ctr = (coeffs + h*efun(x,y,z))*h/al(4);
                rhs(node) = rhs(node) - ctr * gamfun(side,x,y,z);
                ctr = bfun(x,y+hhalf,z) - ctr * betfun(side,x,y,z);
                coeffs = bfun(x,y-hhalf,z);
                a(iau(node)) = a(iau(node)) - coeffs + ctr;
                a(nbr) = 2.*coeffs;
            end
        end
    end
    //
    //     If only has 2. dimesion to work on, return now
    //
    if (nz <= 1) then
        return
    end
    //
    //     The front side boundary
    //
    //     If the edges of the surface has been decided by Dirichlet Boundary
    //     Condition, then leave them alone.
    //
    if (al(3) == 0. ) then
        ly = 2
    else
        ly = 1
    end
    if (al(4) == 0. ) then
        uy = ny-1
    else
        uy = ny
    end
    //
    z = 0. 
    side = 'z1'
    for j = ly: uy
        y = (j-1)*h;
        for i = lx: ux
            x = (i-1)*h;
            node = i + (j-1)*ky;
            //
            if (al(5) == 0. ) then
                a = clrow(node, a, ia);
                a(iau(node)) = betfun(side,x,y,z);
                rhs(node) = gamfun(side,x,y,z);
            else
                nbr = lctcsr(node, node+kz, ja, ia);
                //
                coeffs = 2.*cfun(x,y,z);
                ctr = (h*ffun(x,y,z) - coeffs)*h/al(5);
                rhs(node) = rhs(node) + ctr * gamfun(side,x,y,z);
                ctr = cfun(x,y,z-hhalf) + ctr * betfun(side,x,y,z);
                coeffs = cfun(x,y,z+hhalf);
                a(iau(node)) = a(iau(node)) - coeffs + ctr;
                a(nbr) = 2.*coeffs;
            end
        end
    end
    //
    //     Similiarly for the top side of the boundary suface
    //
    z = (nz - 1) * h
    side = 'z2'
    for j = ly: uy
        y = (j-1)*h;
        for i = lx: ux
            x = (i-1)*h;
            node = (nz-1)*kz + (j-1)*ky + i;
            //
            if (al(6) == 0. ) then
                a = clrow(node, a, ia);
                a(iau(node)) = betfun(side,x,y,z);
                rhs(node) = gamfun(side,x,y,z);
            else
                nbr = lctcsr(node, node-kz, ja, ia);
                //
                coeffs = 2.*cfun(x,y,z);
                ctr = (coeffs + h*ffun(x,y,z))*h/al(6);
                rhs(node) = rhs(node) - ctr * gamfun(side,x,y,z);
                ctr = cfun(x,y,z+hhalf) - ctr * betfun(side,x,y,z);
                coeffs = cfun(x,y,z-hhalf);
                a(iau(node)) = a(iau(node)) - coeffs + ctr;
                a(nbr) = 2.*coeffs;
            end
        end
    end
endfunction

function a = clrow(i, a, ia)
    //-----------------------------------------------------------------------
    //     clear the row i to all 0. , but still keep the structure of the
    //     CSR matrix
    //-----------------------------------------------------------------------
    for k = ia(i): ia(i+1)-1
        a(k) = 0.
    end
endfunction
//-----------------------------------------------------------------------
function loc = lctcsr(i,j,ja,ia)
    //-----------------------------------------------------------------------
    //     locate the position of a matrix element in a CSR format
    //     returns -1 if the desired element is 0. 
    //-----------------------------------------------------------------------
    loc = -1
    k = ia(i)
    while (k < ia(i+1) & (loc == -1))
        if (ja(k) == j) then
            loc = k
        end
        k = k + 1
    end
endfunction


