// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function bandwidthdemo()
    path  = get_absolute_file_path("bandwidthsum.sce");
    exec(fullfile(path,"bandwidthsum.sci"));

    mprintf("Testing bandwidthsum\n");
    mprintf("See demos/bandwidthsum.sci for details.\n");

    // A 6-by-6 sparse matrix
    // nnz(A) = 16
    A=[
    -1.    3.    0.    0.    4.    0.  
    2.   -5.    0.    0.    0.    1.  
    0.    0.   -2.    3.    0.    0.  
    0.    0.    7.   -1.    0.    0.  
    -3.    0.    0.    4.    6.    0.
    0.    5.    0.    0.   -7.    8. 
    ];
    A=sparse(A);
    bd = spilu_bandwidthsum(A);
    assert_checkequal(bd,25);

    //
    // Load this script into the editor
    //
    filename = "bandwidthsum.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
    editor ( fullfile(dname,"bandwidthsum.sci") );
endfunction
bandwidthdemo();
clear bandwidthdemo;
