// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================



function [A,k] = spilu_decomprand(n,__fundecomp__,__randgen__,kmax)
    // Search for a random sparse matrix sensitive to decomposition parameters.
    // 
    // Calling Sequence
    // [A,k] = spilu_decomprand(n,fundecomp,randgen,kmax)
    //
    // Parameters
    // n: a 1-by-1 matrix of doubles, integer value, the size of the required matrix
    // fundecomp: a list, the decomposition algorithm
    // randgen: a list, the random matrix generator
    // kmax: a 1-by-1 matrix of doubles, integer value, the maximum number of random trials
    // A: a n-by-n sparse matrix
    // k : a 1-by-1 matrix of doubles, integer value, the actual number of random trials
    //
    // Description
    // Searches for a random sparse matrix which is sensitive to both the 
    // decomposition parameters of the fundecomp algorithm.
    // The random matrices are produces with the 
    // spilu_spspdrand function.
    //
    // If no matrix is found in kmax trials, produces an error.
    //
    // The function fundecomp is a list (f,a1,a2,...,an), 
    // where the first element f is a function with header:
    //
	// <screen>
    // tf = f(A,a1,a2,...,an)
	// </screen>
    //
    // where A is the sparse matrix, tf is true if matrix is sensitive 
    // to decomposition parameters and a1, a2, ..., an are extra-arguments 
    // which are automatically added to the calling sequence of f.
    //
    // The function randgen is a list (g,a1,a2,...,an), 
    // where the first element f is a function with header:
    //
	// <screen>
    // A = g(n,a1,a2,...,an)
	// </screen>
    //
    // where n is the size of the matrix, A is the random sparse matrix 
    // and a1, a2, ..., an are extra-arguments 
    // which are automatically added to the calling sequence of g.
    //
    // Examples
    // function tf = isIludSensitive(A)
    //     tf = %f
    //     //
    //     // Search initial decomposition
    //     instr = "[L0,U0]=spilu_ilud(A)";
    //     ierr = execstr(instr,"errcatch");
    //     if ( ierr <> 0 ) then
    //         return
    //     end
    //     //
    //     // See if alpha is sensitive
    //     [L,U]=spilu_ilud(A,1.);
    //     tfL = and(L0==L);
    //     tfU = and(U0==U);
    //     if ( tfL | tfU ) then
    //         return
    //     end
    //     //
    //     // See if drop is sensitive
    //     [L,U]=spilu_ilud(A,[],1.);
    //     tfL = and(L0==L);
    //     tfU = and(U0==U);
    //     if ( ~tfL & ~tfU ) then
    //         tf = %t
    //     end
    // endfunction
    // // Find a 5-by-5 sparse matrix, with integer entries in [-10,10], 
    // // such that ilud is sensitive to the alpha and drop parameters. 
    // // Perform 100 trials.
    // [A,k] = spilu_decomprand(5,list(isIludSensitive),..
    //     list(spilu_spspdrand,-10,10,2.),100)
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

	[lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_decomprand" , rhs , 4 )
    apifun_checklhs ( "spilu_decomprand" , lhs , 0:2 )
    //
    // Check type
    apifun_checktype ( "spilu_decomprand" , n , "n" , 1 , "constant" )
    apifun_checktype ( "spilu_decomprand" , __fundecomp__ , "fundecomp" , 2 , "list" )
    apifun_checktype ( "spilu_decomprand" , __randgen__ , "randgen" , 3 , "list" )
    apifun_checktype ( "spilu_decomprand" , kmax , "kmax" , 4 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "spilu_decomprand" , n , "n" , 1 )
    apifun_checkscalar ( "spilu_decomprand" , kmax , "kmax" , 4 )
    //
    // Check content
    apifun_checkgreq ( "spilu_decomprand" , n , "n" , 1 , 1 )
    apifun_checkgreq ( "spilu_decomprand" , kmax , "kmax" , 4 , 1 )
	//
	// Proceed...

    A = []
    __fundecomp_f__ = __fundecomp__(1)
    __randgen_f__ = __randgen__(1)

    for k = 1 : kmax
        B = __randgen_f__(n,__randgen__(2:$))
        tf = __fundecomp_f__(B,__fundecomp__(2:$))
        if ( tf ) then
            A = B
            break
        end
    end
    if ( A==[] ) then
        error(msprintf("%s: No matrix found.","spilu_decomprand"))
    end
endfunction
