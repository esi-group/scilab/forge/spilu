// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// Test gen57pt

function genmatdemo()
    path  = get_absolute_file_path("genmat.sce");
    exec(fullfile(path,"genmat.sci"));
    exec(fullfile(path,"functns.sci"));

    mprintf("Test gen57pt\n");
    mprintf("See demos/genmat.sci for details.\n");

    nx = 5;
    ny = 5;
    nz = 1;
    //     
    //     define part of the boundary condition here
    //     
    al(1) = 0.0;
    al(2) = 1.0;
    al(3) = 0.0;
    al(4) = 0.0;
    al(5) = 0.0;
    al(6) = 0.0;
    mmode = 0;
    [n,a,ja,ia,iau,rhs] = gen57pt(nx,ny,nz,al,mmode);
    n_expected = 12;
    assert_checkequal(n,n_expected);
    ia_expected = [
    1
    4
    8
    12
    15
    19
    24
    29
    33
    36
    40
    44
    47
    56
    61
    65
    69
    74
    79
    84
    88
    91
    95
    99
    103
    106
    ];
    assert_checkequal(ia,ia_expected);
    a_expected = [
    4.
    0.25 
    -1.
    -2.25
    4.
    0.25 
    -1.
    -2.25
    4.
    0.25 
    -1.
    -2.
    3.875
    -1.
    -1.
    4.
    0.25 
    -1.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.
    -1.
    3.875
    -1.
    -1.
    4.
    0.25 
    -2.25
    -1.
    4.
    0.25 
    -2.25
    -1.
    4.
    0.25 
    -2.
    -1.
    3.875
    -1.
    4.
    0.25 
    -1.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.
    -1.
    3.875
    -1.
    0.
    1.
    0.
    0.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.25
    -1.
    4.
    0.25 
    -1.
    -2.
    -1.
    3.875
    -1.
    0.
    1.
    0.
    0.
    0.
    1.
    0.
    0.
    0.
    1.
    0.
    0.
    0.
    1.
    0.
    0.
    0.
    1.
    ];
    assert_checkequal(a,a_expected);
    ja_expected = [
    1
    2
    5
    1
    2
    3
    6
    2
    3
    4
    7
    3
    4
    8
    1
    5
    6
    9
    5
    2
    6
    7
    10
    6
    3
    7
    8
    11
    7
    4
    8
    12
    5
    9
    10
    9
    6
    10
    11
    10
    7
    11
    12
    11
    8
    12
    7
    12
    13
    17
    12
    8
    13
    14
    18
    13
    9
    14
    15
    19
    14
    10
    15
    20
    11
    16
    17
    21
    16
    12
    17
    18
    22
    17
    13
    18
    19
    23
    18
    14
    19
    20
    24
    19
    15
    20
    25
    16
    21
    22
    21
    17
    22
    23
    22
    18
    23
    24
    23
    19
    24
    25
    24
    20
    25
    ];
    assert_checkequal(ja,ja_expected);
    // Convert L, U from CRS to sparse
    A=adj2sp(ia,ja,a)';

    //
    // Load this script into the editor
    //
    filename = "genmat.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
    editor ( fullfile(dname,"genmat.sci") );
endfunction
genmatdemo();
clear genmatdemo;
