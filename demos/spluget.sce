//
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This toolbox is released under the terms of the CeCILL license :
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

function splugetdemo()

    path  = get_absolute_file_path("spluget.sce");
    exec(fullfile(path,"spluget.sci"));

	mprintf("Testing spluget\n");
	mprintf("See demos/spluget.sci for details.\n");
    n = 3;
    ju = [
    5.
    7.
    9.
    ];
    jlu  = [
    5.
    6.
    8.
    9.
    2.
    1.
    3.
    2.
    ];
    alu  = [
    1.
    -0.5
    1/22
    0.
    2.
    3.
    5.
    -3.
    ];
    // Convert the L and U factors from MSR into CSR
    [Lrow_ptr, Lcol_ind, Lnnz, Lval, ...
    Urow_ptr, Ucol_ind, Unnz, Uval]= spilu_spluget(n,ju,jlu,alu);
    // Convert L, U from CRS to sparse
    L=adj2sp(Lrow_ptr,Lcol_ind,Lval)';
    U=adj2sp(Urow_ptr,Ucol_ind,Uval)';
    //
    // Expected values
    Le = [
    1  0 0
    3  1 0
    0 -3 1
    ];
    Ue = [
    1  2 0
    0 -2 5
    0  0 22
    ];
    assert_checkequal(full(L),Le);
    assert_checkequal(full(U),Ue);

    //
    // Load this script into the editor
    //
    filename = "spluget.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
    editor ( fullfile(dname,"spluget.sci") );

endfunction
splugetdemo();
clear splugetdemo;
