// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function spspdrandDemo()

    mprintf("Testing spspdrand\n")
    mprintf("See demos/spspdrand.sci for details.\n")

    path  = get_absolute_file_path("spspdrand.sce");
    exec(fullfile(path,"spspdrand.sci"));

    A = spilu_spspdrand(5,-10,10,2.);
    Aoffdiag = A - diag(diag(A));
    assert_checkequal(size(A),[5 5]);
    assert_checktrue(full(Aoffdiag)<=10);
    assert_checktrue(full(Aoffdiag)>=-10);
    // Check semi-definite positive
    Adiag = diag(diag(A));
    SAO = sum(full(Aoffdiag),"c");
    SAD = sum(full(Adiag),"c");
    assert_checktrue(SAD>=SAO);

    //
    // Load this script into the editor
    //
    filename = "spspdrand.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
    editor ( fullfile(dname,"spspdrand.sci") );
endfunction 
spspdrandDemo();
clear spspdrandDemo;


