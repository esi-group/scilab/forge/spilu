// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

function PDE225Demo()

    mprintf("See the LU factorizations in action on PDE225 matrix.\n")

    path = spilu_getpath (  );
    filename = fullfile(path,"tests","matrices","pde225.mtx");
    A = mmread(filename);
    nrows = size(A,"r");
    ncols = size(A,"c");
    mprintf("Matrix Sparse : %d-by-%d\n",nrows,ncols);
    mprintf("nnz(A)=%d\n",nnz(A));
	
	// Create a RHS
    n = size(A,"r");
    x = (1:n)';
    b = A*x;

    // Without Precond
    [xcomp, err, iterWO] = imsls_gmres( A, b);
    iterExp(1)=iterWO;
    mprintf("Preconditioner #%d: No Preconditionner\n",1)
    digits = assert_computedigits(x,xcomp);
    digits = min(digits);
    mprintf("    Digits in X = %d\n", digits );
    mprintf("    Final relative residual norm = %d\n", err );
    mprintf("    Iterations = %d\n", iterWO );

    marray = spilu_iluhubavail();
    nexp = size(marray,"*");
    for k = 1 : nexp
        m = marray(k);
        mprintf("Preconditioner #%d: %s\n",k+1,m)
        [L,U,perm]=spilu_iluhub(A,m);
        mprintf("    nnz(A) = %d\n", nnz(A))
        nnzExp(k) = nnz(L)+nnz(U);
        mprintf("    nnz(LU) = %d\n", nnzExp(k));
        P = spilu_permVecToMat(perm);
        [xcomp, err, iterK] = imsls_gmres( P*A, P*b, [], L, U);
        digits = assert_computedigits(x,xcomp);
        digits = min(digits);
        mprintf("    Digits in X = %d\n", digits );
        mprintf("    Final relative residual norm = %d\n", err );
        mprintf("    Iterations = %d\n", iterK );
        iterExp(k+1)=iterK;
    end

    // 
    // Plot the number of nonzeros depending 
    // on the preconditionner.
    h = scf();
    bar(nnzExp);
    xtitle("Matrix PDE225","Preconditionner","NNZ(L)+NNZ(U)");
    for k = 1 : nexp
        m = marray(k);
        h.children.x_ticks.labels(k) = m;
    end

    // 
    // Plot the number of iterations depending 
    // on the preconditionner.
    h = scf();
    bar(iterExp);
    xtitle("Matrix PDE225 - GMRES","Preconditionner","Number of iterations");
    h.children.x_ticks.labels(1) = "No Precond.";
    for k = 1 : nexp
        m = marray(k);
        h.children.x_ticks.labels(k+1) = m;
    end

    //
    // Plot the sensitivity of GMRES with and without Preconditionning.
    //
    // Without precond.
    [xcomp, err, iter, flag, resWO] = imsls_gmres( A, b);
    //
    // With ILUDP
    [L,U,perm]=spilu_iludp(A);
    P = spilu_permVecToMat(perm);
    [xcomp, err, iter, flag, resILUTP] = imsls_gmres( P*A, P*b, [], L, U);
    //
    // With ILU0
    [L,U]=spilu_ilu0(A);
    [xcomp, err, iter, flag, resILU0] = imsls_gmres( A, b, [], L, U);
    //
    h = scf();
    plot(resWO,"b*--");
    plot(resILU0,"r+-");
    plot(resILUTP,"go-.");
    xtitle("Matrix PDE225 - GMRES","Iterations","Residual");
    h.children.log_flags = "lln";
    legend(["Without Precond.","With ILU0","With ILUTP"]);

    //
    // Load this script into the editor
    //
    filename = "demoPDE225.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
endfunction 
PDE225Demo();
clear PDE225Demo;
