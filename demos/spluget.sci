//
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This toolbox is released under the terms of the CeCILL license :
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//


function [Lrow_ptr, Lcol_ind, Lnnz, Lval, Urow_ptr, Ucol_ind, Unnz, Uval]= spilu_spluget(n,ju,jlu,alu)
    // Convert the MSR of the LU factors of A into CSR
    // 
    // Calling Sequence
    // [Lrow_ptr, Lcol_ind, Lnnz, Lval, Urow_ptr, Ucol_ind, Unnz, Uval]= spilu_spluget(n,ju,jlu,alu)
    //
    // Parameters
    // n: a 1-by-1 matrix of doubles, the size of the matrix
    // ju: a n-by-1 matrix of doubles, pointer to the diagonal elements in alu, jlu
    // alu: a (nnz+1)-by-1 matrix of doubles, the values of the ALU factors
    // jlu: a (nnz+1)-by-1 matrix of doubles, pointers to the alu array
    //
    // Description
    // Convert the  Modified Sparse Row (MSR) format
    // into Compressed Sparse Row (CSR) format.
    // We make the assumption that A is square, 
    // i.e. is a n-by-n sparse matrix.
    //
    // This is a Scilab port of the C function spluget in src/c/conv.c.
    // The goal of the spluget function is to reproduce the steps of the 
    // compiled source code executed in the Scilab gateway.
    //
    // The diagonal of the L is unity, and this is why it is 
    // not stored. 
    // The L and U factors are stored in one single matrix:
    //
    // <screen>
    // LU = 
    //     U(1,1) U(1,2) U(1,3) ... U(1,n)
    //     L(2,1) U(2,2) U(2,3) ... U(2,n)
    //     ...
    //     L(n,1) L(n,2)    ... ... U(n,n)
    // </screen>
    //
    // This LU matrix is sparse and is stored in MSR format.
    //
    // The Modified Sparse Row (MSR) format is a 
    // variation of the Compressed Sparse Row format which consists 
    // of keeping the main diagonal of the LU factors separately. 
    // The corresponding data structure consists of a real array alu
    // and two integer arrays jlu and ju.
    //
    // The alu array is made of two parts:
    // <itemizedlist>
    //     <listitem><para>
    //         alu(1:n) is the inverted diagonal of the U factors. 
    //     </para></listitem>
    //     <listitem><para>
    //         alu(n+1) is not used. 
    //     </para></listitem>
    //     <listitem><para>
    //         alu(n+2:$) : the nonzero elements of the LU factors, 
    //         excluding its diagonal elements, are stored row-wise.
    //     </para></listitem>
    // </itemizedlist>
    //
    // The jlu array is made of two parts:
    // <itemizedlist>
    //     <listitem><para>
    //         jlu(1:n) : jlu(k) is the begininning of the k-th row in alu,
    //         i.e. alu(jlu(k)) is the first nonzero of the k-th row of LU.
    //     </para></listitem>
    //     <listitem><para>
    //         jlu(n+1) is not used.
    //     </para></listitem>
    //     <listitem><para>
    //         jlu(n+2:$) : jlu(k) is the column index of the element alu(k).
    //     </para></listitem>
    // </itemizedlist>
    //
    // The ju array separates the L and U parts in each row:
    // <itemizedlist>
    //     <listitem><para>
    //         ju(1:n) : ju(k) is the indice in alu where the U factors starts,
    //              i.e. alu(ju(k)) is the first nonzero of the k-th row of U.
    //     </para></listitem>
    // </itemizedlist>
    //
    // Examples
    // n = 3;
    // ju = [
    //     5.  
    //     7.  
    //     9.  
    // ];
    //  jlu  = [
    //     5.  
    //     6.  
    //     8.  
    //     9.  
    //     2.  
    //     1.  
    //     3.  
    //     2.  
    // ];
    //  alu  = [ 
    //     1.         
    //    -0.5        
    //     1/22
    //     0.         
    //     2.         
    //     3.         
    //     5.         
    //    -3.         
    // ];
    // // Convert the L and U factors from MSR into CSR
    // [Lrow_ptr, Lcol_ind, Lnnz, Lval, ...
	//     Urow_ptr, Ucol_ind, Unnz, Uval]= spilu_spluget(n,ju,jlu,alu)
    // // Convert L, U from CRS to sparse
    // L=adj2sp(Lrow_ptr,Lcol_ind,Lval)'
    // U=adj2sp(Urow_ptr,Ucol_ind,Uval)'
    // //
    // // Expected values
    // Le = [
    // 1  0 0
    // 3  1 0
    // 0 -3 1
    // ];
    // Le = sparse(Le);
    // Ue = [
    // 1  2 0
    // 0 -2 5
    // 0  0 22
    // ];
    // Ue = sparse(Ue);
    //
    // Bibliography
    //  "SPARSKIT: A basic tool-kit for sparse matrix computations", Youcef Saad, 1994, section 2.1.1 Compressed Sparse Row and related formats
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

	[lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_spluget" , rhs , 4 )
    apifun_checklhs ( "spilu_spluget" , lhs , 8:8 )
    //
    // Check type
    apifun_checktype ( "spilu_spluget" , n , "n" , 1 , "constant" )
    apifun_checktype ( "spilu_spluget" , ju , "ju" , 2 , "constant" )
    apifun_checktype ( "spilu_spluget" , jlu , "jlu" , 3 , "constant" )
    apifun_checktype ( "spilu_spluget" , alu , "alu" , 4 , "constant" )
	//
	// Check size
    apifun_checkscalar ( "spilu_spluget" , n , "n" , 1 )
    apifun_checkvector ( "spilu_spluget" , ju , "ju" , 2 , n )
    apifun_checkvector ( "spilu_spluget" , jlu , "jlu" , 3 )
	nnzjlu = size(jlu,"*")
    apifun_checkvector ( "spilu_spluget" , alu , "alu" , 4 , nnzjlu )
	//
	// Check content
    apifun_checkgreq ( "spilu_spluget" , n , "n" , 1 , 1 )
    apifun_checkgreq ( "spilu_spluget" , ju , "ju" , 2 , 1 )
    apifun_checkgreq ( "spilu_spluget" , jlu , "jlu" , 3 , 1 )
    apifun_checkloweq ( "spilu_spluget" , ju , "ju" , 2 , n^2 )
    apifun_checkloweq ( "spilu_spluget" , jlu , "jlu" , 3 , n^2 )
	//
	// Proceed...
	
    // Create sparse L
    Lm=n; // Number of rows
    Ln=n; // Number of columns
    Lnnz=0; // Number of nonzeros

    // Create sparse U
    Um=n;   // Number of rows
    Un=n;   // Number of columns
    Unnz=0; // Number of nonzeros

    for i=1:n
        Lmnel(i)=ju(i)-jlu(i)+1;
        Lnnz=Lnnz + Lmnel(i);
        Umnel(i)=jlu(i+1)-ju(i)+1;
        Unnz=Unnz + Umnel(i);
    end

    // Compute the pointer Lrow_ptr of the begining of 
    // each row in Lval.
    // Compute this depending on Lmnel.
    Lrow_ptr(1) = 1
    for i = 2:n+1
        Lrow_ptr(i) = Lrow_ptr(i-1) + Lmnel(i-1)
    end
    // The same for U
    Urow_ptr(1) = 1
    for i = 2:n+1
        Urow_ptr(i) = Urow_ptr(i-1) + Umnel(i-1)
    end

    // Initializations
    indLU=n+2
    indL=1
    indU=1

    for row=1:n
        for i=1:Lmnel(row)-1
            Lcol_ind(indL)=jlu(indLU);
            Lval(indL)=alu(indLU);
            indL = indL + 1;
            indLU = indLU + 1;
        end

        Lcol_ind(indL)=row;
        Lval(indL)=1;
        indL = indL + 1;

        Ucol_ind(indU)=row;
        Uval(indU)=1/(alu(row));
        indU = indU + 1;

        for i=1:Umnel(row)-1
            Ucol_ind(indU)=jlu(indLU);
            Uval(indU)=alu(indLU);
            indU = indU + 1;
            indLU = indLU + 1;
        end
    end

endfunction

