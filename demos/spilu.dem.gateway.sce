// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

demopath = get_absolute_file_path("spilu.dem.gateway.sce");
subdemolist = [
"Benchmark: GMRES", "rilut.sce"; ..
"Benchmark: West0479", "demoWest0479.sce"; ..
"Benchmark: PDE225", "demoPDE225.sce"; ..
"Benchmark: genmat", "genmat.sce"; ..
"Support: bandwidthsum", "bandwidthsum.sce"; ..
"Support: decomprand", "decomprand.sce"; ..
"Support: spluget", "spluget.sce"; ..
"Testing: ilu0Fortran", "ilu0Fortran.sce"; ..
"Testing: ilud_searchForMatrix", "ilud_searchForMatrix.sce"; ..
"Testing: iludp_searchForMatrix", "iludp_searchForMatrix.sce"; ..
"Testing: iluk_searchForMatrix", "iluk_searchForMatrix.sce"; ..
"Testing: ilut_searchForMatrix", "ilut_searchForMatrix.sce"; ..
"Testing: spspdrand", "spspdrand.sce"; ..
"Tutorial: sparseStorageFormats", "sparseStorageFormats.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
