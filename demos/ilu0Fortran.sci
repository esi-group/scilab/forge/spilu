// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) Yousef Saad
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [alu, jlu, ju, iw, ierr] = ilu0Fortran(a, ja, ia)
    //
    // This is a Scilab port of the Fortran function ilu0 in src/fortran/ilu0.f.
    // The spilu_ilu0M function is a better implementation of the same algorithm.
    // The goal of the ilu0Fortran function is to reproduce the steps of the 
    // compiled source code executed in the Scilab gateway.


    //------------------ right preconditioner ------------------------------*
    //                    ***   ilu(0) preconditioner.   ***                *
    //----------------------------------------------------------------------*
    // Note that this has been coded in such a way that it can be used
    // with pgmres. Normally, since the data structure of the L+U matrix is
    // the same as that the A matrix, savings can be made. In fact with
    // some definitions (not correct for general sparse matrices) all we
    // need in addition to a, ja, ia is an additional diagonal.
    // ILU0 is not recommended for serious problems. It is only provided
    // here for comparison purposes.
    //-----------------------------------------------------------------------
    //
    // on entry:
    //---------
    // a, ja,
    // ia      = original matrix in compressed sparse row storage.
	//
	// Some extra comments.
	// a(1:nnz): the nonzeros of A.
	//           For i=1, 2,..., nnz, the value a(i) is the i-th nonzero of A.
	//           This is the "val" array in the CSR format.
	// ia(1:n+1): pointer to the rows of A.
	//            For i=1, 2,..., n, the value ia(i) is the first 
	//            index of a for the row A(i,:).
	//            In other words, for i=1, 2,..., n, 
	//            a(ia(i):ia(i+1)-1) contains the nonzeros of the i-th row.
	//            The number of nonzeros is : ia(n+1)-1
	//            This is the "row_ptr" array in the CSR format.
	// ja(1:nnz): the column indices of A.
	//            For i=1,2,...nnz, ja(i) is the column index of the 
	//            i-th nonzero of A.
	//            This is the "col_ind" array in the CSR format.
    //
    // on return:
    //-----------
    // alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
    //           the L and U factors together. The diagonal (stored in
    //           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
    //           contains the i-th row of L (excluding the diagonal entry=1)
    //           followed by the i-th row of U.
    //
    // ju      = pointer to the diagonal elements in alu, jlu.
	//
	// Some extra comments.
    // ju(1:n): pointer to the diagonal elements in alu, jlu
    // alu(1:nnz+1): the values of the ALU factors
    // jlu(1:nnz+1): pointers to the alu array
    //
    // ierr      = integer indicating error code on return
    //         ierr = 0 --> normal return
    //         ierr = k --> code encountered a zero pivot at step k.
    // work arrays:
    //-------------
    // iw        = integer work array of length n.
    //------------
    // IMPORTANT
    //-----------
    // it is assumed that the the elements in the input matrix are stored
    //    in such a way that in each row the lower part comes first and
    //    then the upper part. To get the correct ILU factorization, it is
    //    also necessary to have the elements of L sorted by increasing
    //    column number. It may therefore be necessary to sort the
    //    elements of a, ja, ia prior to calling ilu0. This can be
    //    achieved by transposing the matrix twice using csrcsc.
    //
    //-----------------------------------------------------------------------
    nnzA = size(a,"*")
    n = size(ia,"*")-1
    ju = zeros(n,1)
    alu = zeros(nnzA+1,1)
    jlu = zeros(nnzA+1,1)
    ju0 = n+2
    jlu(1) = ju0
    //
    // initialize work vector to zero's
    //
    for i=1:n
        iw(i) = 0
    end
    //
    // main loop
    //
    for  ii = 1: n
        js = ju0
        //
        // generating row number ii of L and U.
        //
        for  j=ia(ii):ia(ii+1)-1
            //
            //     copy row ii of a, ja, ia into row ii of alu, jlu (L/U) matrix.
            //
            jcol = ja(j)
            if (jcol == ii) then
                alu(ii) = a(j)
                iw(jcol) = ii
                ju(ii)  = ju0
            else
                alu(ju0) = a(j)
                jlu(ju0) = ja(j)
                iw(jcol) = ju0
                ju0 = ju0+1
            end
        end
        jlu(ii+1) = ju0
        jf = ju0-1
        jm = ju(ii)-1
        //
        //     exit if diagonal element is reached.
        //
        for j=js: jm
            jrow = jlu(j)
            tl = alu(j)*alu(jrow)
            alu(j) = tl
            //
            //     perform  linear combination
            //
            for jj = ju(jrow): jlu(jrow+1)-1
                jw = iw(jlu(jj))
                if (jw <> 0) then
                    alu(jw) = alu(jw) - tl*alu(jj)
                end
            end
        end
        //
        //     invert  and store diagonal element.
        //
        if (alu(ii) == 0.) then
            //
            //     zero pivot :
            //
            ierr = ii
            return
        end
        alu(ii) = 1. / alu(ii)
        //
        //     reset pointer iw to zero
        //
        iw(ii) = 0
        for  i = js: jf
            iw(jlu(i)) = 0
        end
    end
    ierr = 0
endfunction

