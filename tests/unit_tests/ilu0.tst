// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

//
// Test error cases
A = [
1 2 0 0 0
3 4 5 0 0
0 6 7 8 0
];
instr = "[L,U]=spilu_ilu0(A)";
name="spilu_ilu0";
msg1="%s: input matrix must be sparse.";
assert_checkerror(instr, msg1, [], name);
//
A = sparse(A);
//
instr = "[L,U]=spilu_ilu0(A)";
name="spilu_ilu0";
msg1="%s: input matrix must be square.";
assert_checkerror(instr, msg1, [], name);
//
// Simplest possible test 3-by-3 test case
// n = 3
// nnz = 7
A = [
1 2 0
3 4 5
0 6 7
];
A = sparse(A);
[L,U]=spilu_ilu0(A);
Le = [
1  0 0
3  1 0
0 -3 1
];
Le = sparse(Le);
Ue = [
1  2 0
0 -2 5
0  0 22
];
Ue = sparse(Ue);
assert_checkequal(L,Le);
assert_checkequal(U,Ue);
//
// A 6-by-6 test case
// n = 6
// nnz = 16
A=[
-1.    3.    0.    0.    4.    0.
2.    -5.    0.    0.    0.    1.
0.     0.   -2.    3.    0.    0.
0.     0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.     5.    0.    0.   -7.    8.
];
A=sparse(A);
//
expL=[
1.     0.    0.     0.           0.           0.
-2.    1.    0.     0.           0.           0.
0.     0.    1.     0.           0.           0.
0.     0.   -3.5    1.           0.           0.
3.     0.    0.     0.4210526    1.           0.
0.     5.    0.     0.           1.1666667    1.
];
expU=[
-1.    3.    0.    0.     4.    0.
0.     1.    0.    0.     0.    1.
0.     0.   -2.    3.     0.    0.
0.     0.    0.    9.5    0.    0.
0.     0.    0.    0.    -6.    0.
0.     0.    0.    0.     0.    3.
];
[L,U]=spilu_ilu0(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//
// A 6-by-6 test case
// Zero pivot error
// n = 6
// nnz = 15
A=[
0.     3.    0.    0.    4.    0.
2.    -5.    0.    0.    0.    1.
0.     0.   -2.    3.    0.    0.
0.     0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.     5.    0.    0.   -7.    8.
];
A=sparse(A);
instr = "[L,U]=spilu_ilu0(A)";
msg1="%s: zero pivot encountered at step number %d.";
assert_checkerror ( instr, msg1, [], "spilu_ilu0",1);
