// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

path = spilu_getpath (  );
exec(fullfile(path,"demos","spspdrand.sci"));

// Test with random matrices:
// All these decompositions must work

function A = myspdrand(n,a,b)
  A = spilu_spspdrand(n,a,b,0.)
  A = A + speye(n,n)
endfunction

// These solvers needs matrices invertible
// without pivoting:
// [L,U]=spilu_ilu0(A);
// [L,U]=spilu_iluk(A);
// [L,U]=spilu_milu0(A);
for k = 1  : 100
  A = myspdrand(5,-10,10);
  [L,U]=spilu_ilud(A);
  [L,U,perm]=spilu_iludp(A);
  [L,U]=spilu_ilut(A);
  [L,U,perm]=spilu_ilutp(A);
end

for k = 1  : 10
  A = myspdrand(100,-10,10);
  [L,U]=spilu_ilud(A);
  [L,U,perm]=spilu_iludp(A);
  [L,U]=spilu_ilut(A);
  [L,U,perm]=spilu_ilutp(A);
end

for k = 1  : 100
  A = spilu_spspdrand(5,-10,10,2.);
  [L,U]=spilu_ilu0(A);
  [L,U]=spilu_iluk(A);
  [L,U]=spilu_milu0(A);
  [L,U]=spilu_ilud(A);
  [L,U,perm]=spilu_iludp(A);
  [L,U]=spilu_ilut(A);
  [L,U,perm]=spilu_ilutp(A);
end

for k = 1  : 10
  A = spilu_spspdrand(100,-10,10,2.);
  [L,U]=spilu_ilu0(A);
  [L,U]=spilu_iluk(A);
  [L,U]=spilu_milu0(A);
  [L,U]=spilu_ilud(A);
  [L,U,perm]=spilu_iludp(A);
  [L,U]=spilu_ilut(A);
  [L,U,perm]=spilu_ilutp(A);
end
