// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

// A small 3-by-3 matrix
// nnz(A)=7
A = [
1 2 0
3 4 5
0 6 7
];
A = sparse(A);
[L,U,perm]=spilu_iluhub(A,"ilu0");
perm_expected = [1 2 3];
U_expected = [
    1.    2.    0.
    0.   -2.    5.
    0.    0.    22.
];
L_expected = [
    1.    0.    0.
    3.    1.    0.
    0.   -3.    1.
];
assert_checkequal(perm,perm_expected);
assert_checkequal(full(U),U_expected);
assert_checkequal(full(L),L_expected);

// Test ilud
alph = 0.1;
drop = 0.001;
[L,U,perm]=spilu_iluhub(A,"ilud",alph,drop)

//
// Test all methods
marray = spilu_iluhubavail();
for m = marray
    mprintf("Method: %s\n",m);
    [L,U,perm]=spilu_iluhub(A,m);
    mprintf("    nnz(LU)= %d\n",nnz(L)+nnz(U));
end
