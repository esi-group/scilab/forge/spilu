// ====================================================================
// Copyright (C) 2011 -NII -Benoit Goepfert
// Copyright (C) 2011 -Digiteo -Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);

//default
expL=[
1.    0.    0.     0.           0.           0.
-2.    1.    0.     0.           0.           0.
0.    0.    1.     0.           0.           0.
0.    0.   -3.5    1.           0.           0.
3.   -9.    0.     0.4210526    1.           0.
0.    5.    0.     0.          -0.7121212    1.
];
expU=[
-1.    3.    0.    0.     4.     0.
0.    1.    0.    0.     8.     1.
0.    0.   -2.    3.     0.     0.
0.    0.    0.    9.5    0.     0.
0.    0.    0.    0.     66.    9.
0.    0.    0.    0.     0.     9.4090909
];
[L,U]=spilu_ilut(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

// Try various values of lfil
mprintf("lfil \t norm(A-L*U) \t nnz(L)+nnz(U)\n");
for lfil = 1:6
  [L,U]=spilu_ilut(A,lfil);
  mprintf("%4d \t %11d \t %13d\n",lfil,..
    norm(A-L*U,"inf"),nnz(L)+nnz(U));
end

//lfil=0
lfil = 0;
instr = "[L,U]=spilu_ilut(A,lfil)";
msg1="%s: not enough memory for matrix U.";
assert_checkerror ( instr, msg1, [], "spilu_ilut");

//lfil=1
lfil=1;
[L,U]=spilu_ilut(A,lfil);
expL=[
    1.    0.    0.     0.    0.           0.
  -2.    1.    0.     0.    0.           0.
    0.    0.    1.     0.    0.           0.
    0.    0.  -3.5    1.    0.           0.
    0.    0.    0.   -4.    1.           0.
    0.    0.    0.     0.  -1.1666667    1.
];
expU=[
  -1.    0.    0.    0.    0.    0.
    0.  -5.    0.    0.    0.    0.
    0.    0.  -2.    0.    0.    0.
    0.    0.    0.  -1.    0.    0.
    0.    0.    0.    0.    6.    0.
    0.    0.    0.    0.    0.    8.
];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=2
lfil=2;
[L,U]=spilu_ilut(A,lfil);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.    0.    0.     0.4210526    1.           0.
    0.  -1.    0.     0.         -0.1666667    1.
];
expU=[
  -1.    0.    0.    0.     4.    0.
    0.  -5.    0.    0.     8.    0.
    0.    0.  -2.    3.     0.    0.
    0.    0.    0.    9.5    0.    0.
    0.    0.    0.    0.   -6.    0.
    0.    0.    0.    0.     0.    8.
];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=3
lfil=3;
[L,U]=spilu_ilut(A,lfil);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=3 drop=0.5
drop=0.5;
lfil=3;
[L,U]=spilu_ilut(A,lfil,drop);
expL=[
    1.    0.    0.     0.    0.           0.
  -2.    1.    0.     0.    0.           0.
    0.    0.    1.     0.    0.           0.
    0.    0.  -3.5    1.    0.           0.
    3.  -9.    0.     0.    1.           0.
    0.    5.    0.     0.  -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     0.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    0.
    0.    0.    0.    0.     0.     8.
];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=3 drop=0.1
drop=0.1;
lfil=3;
[L,U]=spilu_ilut(A,lfil,drop);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
