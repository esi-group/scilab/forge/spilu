// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- Non-regression test for bug 570 -->
//
// <-- Bugzilla URL -->
// http://forge.scilab.org/index.php/p/spilu/issues/570/
//
// <-- Short Description -->
//   ilud and iludp sometimes produce "not enough memory".


// <-- JVM NOT MANDATORY -->

// Not enough memory in ILUDP
A = [
    1.    9.    0.    0.    0.
    9.    1.    9.    0.    0.
    0.    9.    1.    0.    0.
    0.    0.    0.    1.   -3.
    0.    0.    0.   -3.    1.
];
A = sparse(A);
[L,U,perm]=spilu_iludp(A);

// Not enough memory in ILUD
A = [
  1.    2.   -1.    9.   -8.
  2.    1.    0.    0.    0.
 -1.    0.    1.    0.    0.
  9.    0.    0.    1.    0.
 -8.    0.    0.    0.    1.
];
A = sparse(A);
[L,U]=spilu_ilud(A);

