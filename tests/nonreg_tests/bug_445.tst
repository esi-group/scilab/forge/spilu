// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- Non-regression test for bug 445 -->
//
// <-- Bugzilla URL -->
// http://forge.scilab.org/index.php/p/spilu/issues/445/
//
// <-- Short Description -->
//   ilu0 crashes when a zero pivot is encountered


// <-- JVM NOT MANDATORY -->

A=[
   -1.    3.    0.    4.    0.
    2.    0.    0.    0.    1.
    0.    0.   -2.    3.    0.
   -3.    0.    7.   -1.    0.
    0.    5.    0.    4.    6.
];
A=sparse(A);
instr = "[L,U]=spilu_ilu0(A)";
msg1="%s: zero pivot encountered at step number %d.";
assert_checkerror( instr, msg1, [], "spilu_ilu0",2);
//
instr = "[L,U]=spilu_milu0(A)";
msg1="%s: zero pivot encountered at step number %d.";
assert_checkerror( instr, msg1, [], "spilu_milu0",2);


