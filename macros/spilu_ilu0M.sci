// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) Yousef Saad
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [L,U] = spilu_ilu0M(A)
    // ILU(0) preconditioning (macro).
    //
    // Calling Sequence
    //   [L,U] = spilu_ilu0M(A)
    //
    // Parameters
    // A : n-by-n sparse real matrix of doubles
    // L  : n-by-n sparse real matrix of doubles, lower triangular matrix
    // U : n-by-n sparse real matrix of doubles, upper triangular matrix
    //
    // Description
    // Builds an incomplete LU factorization of the sparse matrix <literal>A</literal>.
    //
    // The nonzero pattern in L is the same as the nonzero pattern of the
    // lower part of A.
    // The nonzero pattern in U is the same as the nonzero pattern of the
    // upper part of A.
    //
    // ILU0 is not recommended for realistic problems.
    // It is only provided for comparison purposes.
    //
    // All the diagonal elements of the input matrix must be nonzero.
    //
    // Uses ikj variant of Gaussian Elimination.
    //
    // This function is a macro.
    // The purpose of this function is to reproduce the spilu_ilu0
    // function for small sparse matrices with a simple script.
    // This function is a Scilab port of Youcef Saad's Matlab ilu0
    // function.
    //
    // Examples
    // // A small 3-by-3 matrix
    // // nnz(A)=7
    // A = [
    // 1 2 0
    // 3 4 5
    // 0 6 7
    // ];
    // B = sparse(A);
    // [L,U]=spilu_ilu0M(B);
    // full(L)
    // full(U)
    // Lexpected = [
    // 1  0 0
    // 3  1 0
    // 0 -3 1
    // ];
    // Uexpected = [
    // 1  2 0
    // 0 -2 5
    // 0  0 22
    // ];
    //
    // // Preconditioning of a 237-by-237 matrix.
    // // nnz(A)=1017
    // path = spilu_getpath (  );
    // filename = fullfile(path,"tests","matrices","nos1.mtx");
    // A=mmread(filename);
    // [L,U]=spilu_ilu0M(A);
    // norm(A-L*U)
    //
    // // To edit the code
    // edit spilu_ilu0M
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    // Copyright (C) Yousef Saad

    [lhs, rhs] = argn()
    apifun_checkrhs ( "spilu_ilu0M" , rhs , 1 )
    apifun_checklhs ( "spilu_ilu0M" , lhs , 2:2 )
    //
    // Check types
    apifun_checktype ( "spilu_ilu0M" , A ,  "A" , 1 , "sparse" )
    //
    // Check size
    apifun_checksquare ( "spilu_ilu0M" , A ,  "A" , 1 )
    //
    n = size(A,1) ;
    for i=1:n
        [ii,jj,rr] = find(A(i,:));
        nzr = length(jj);
        p = 1;
        k = jj(p) ;
        while (k < i)
            if ( A(k,k) == 0 ) then
                lclmsg = gettext("%s: zero pivot encountered at step number %d.")
                msg = msprintf(lclmsg,"spilu_ilu0M",i)
                error(msg)
            end
            A(i,k) = A(i,k)/A(k,k);
            piv = A(i,k);
            for j=p+1:nzr
                A(i,jj(j)) = A(i,jj(j)) - piv*A(k,jj(j));
            end
            p = p+1;
            k = jj(p) ;
        end
    end
    L = tril(A,-1) + imsls_spdiags(ones(n,1),0,n,n) ;
    U = triu(A);
endfunction
