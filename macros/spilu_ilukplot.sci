// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [data,hLfil] = spilu_ilukplot(varargin)
    // Plots the sensitivity of ILUK for A.
    // 
    // Calling Sequence
    //   spilu_ilukplot(A)
    //   data = spilu_ilukplot(A)
    //   data = spilu_ilukplot(A,N)
    //   [data,hLfil] = spilu_ilukplot(...)
    //
    // Parameters
    // A: a n-by-n sparse matrix
    // N: a 1-by-1 matrix of doubles, integer value, the number of points in the plot (default N = 100)
    // data: a N-by-6 matrix of doubles, the values of the parameters
    // hLfil: a graphics handle, the lfil plot
    //
    // Description
    // For one particular matrix, plot norm(A-L*U)/norm(A) and nnz(L)+nnz(U), depending on 
    // lfil for the ILUK method.
    //
    // The columns of the array data are:
    // <itemizedlist>
    //     <listitem><para>
    //         data(:,1) : the values of lfil
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,2) : the values of norm(A-L*U)/norm(A) with respect to lfil
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,3) : the values of nnz(L)+nnz(U) with respect to lfil
    //     </para></listitem>
    // </itemizedlist>
    //
    // If the decomposition does not work for one value of the parameter, 
    // then norm(A-L*U)/norm(A) and nnz(L)+nnz(U) are set to Infinity.
    // 
    // Examples
    // A = [
    //     30.    8.     7.     0.    0.  
    //     8.     18.    0.     0.   -1.  
    //     7.     0.     14.    0.    0.  
    //     0.     0.     0.     1.    0.  
    //     0.    -1.     0.     0.    2.  
    // ];
    // A = sparse(A);
    // spilu_ilukplot(A);
    // 
    // // See on a 225-by-225 sparse matrix
    // path = spilu_getpath (  );
    // filename = fullfile(path,"tests","matrices","pde225.mtx");
    // A=mmread(filename);
    // [data,hLfil] = spilu_ilukplot(A);
    // hLfil.children(1).data_bounds(2,1) = 20;
    // hLfil.children(2).data_bounds(2,1) = 20;
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

    function [pNorm,pNnz] = ilukCompute(L,U)
        pNorm = norm(A-L*U,"inf")/norm(A,"inf");            
        pNnz = nnz(L) + nnz(U)
    endfunction

    function [pNorm,pNnz] = ilukDecompLfil(A,lfil)
        instr = "[L,U]=spilu_iluk(A,lfil)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = ilukCompute(L,U)
        end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_ilukplot" , rhs , 1:2 )
    apifun_checklhs ( "spilu_ilukplot" , lhs , 0:2 )
    // 
    // Get arguments
    A = varargin(1)
    N = apifun_argindefault ( varargin , 2 , 100 )
    //
    // Check type
    apifun_checktype ( "spilu_ilukplot" , A , "A" , 1 , "sparse" )
    apifun_checktype ( "spilu_ilukplot" , N , "N" , 2 , "constant" )
    //
    // Check size
    apifun_checksquare ( "spilu_ilukplot" , A , "A" , 1 )
    apifun_checkscalar ( "spilu_ilukplot" , N , "N" , 2 )
    //
    // Check content
    apifun_checkgreq ( "spilu_ilukplot" , N , "N" , 2 , 1 )
    //
    // Proceed...
    //
    // Compute decomposition for various values of lfil
    n = size(A,"r")
    lfil = floor(linspace(0,n,N)')
    hLfil = scf();
    [pNormLfil,pNnzLfil] = spilu_plotparameter(A,"lfil",lfil,list(ilukDecompLfil),%t)
    //
    // Gather the results
    data = [...
    lfil,pNormLfil,pNnzLfil...
    ]
endfunction
