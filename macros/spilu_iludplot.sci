// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [data,hAlpha,hDrop] = spilu_iludplot(varargin)
    // Plots the sensitivity of ILUD for A.
    // 
    // Calling Sequence
    //   spilu_iludplot(A)
    //   data = spilu_iludplot(A)
    //   data = spilu_iludplot(A,N)
    //   [data,hAlpha] = spilu_iludplot(...)
    //   [data,hAlpha,hDrop] = spilu_iludplot(...)
    //
    // Parameters
    // A: a n-by-n sparse matrix
    // N: a 1-by-1 matrix of doubles, integer value, the number of points in the plot (default N = 100)
    // data: a N-by-6 matrix of doubles, the values of the parameters
    // hAlpha: a graphics handle, the alpha plot
    // hDrop: a graphics handle, the drop plot
    //
    // Description
    // For one particular matrix, plot norm(A-L*U)/norm(A) and nnz(L)+nnz(U), depending on 
    // alpha and drop for the ILUD method.
    //
    // The columns of the array data are:
    // <itemizedlist>
    //     <listitem><para>
    //         data(:,1) : the values of alpha
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,2) : the values of norm(A-L*U)/norm(A) with respect to alpha
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,3) : the values of nnz(L)+nnz(U) with respect to alpha
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,4) : the values of drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,5) : the values of norm(A-L*U)/norm(A) with respect to drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,6) : the values of nnz(L)+nnz(U) with respect to drop
    //     </para></listitem>
    // </itemizedlist>
    // 
    // If the decomposition does not work for one value of the parameter, 
    // then norm(A-L*U)/norm(A) and nnz(L)+nnz(U) are set to Infinity.
    // 
    // Examples
    // A = [
    //     34.    4.   -10.    0.  -3.  
    //     4.     24.  -8.     0.    0.  
    //   -10.  -8.     36.    0.    0.  
    //     0.     0.     0.     1.    0.  
    //   -3.     0.     0.     0.    6.  
    // ];
    // A = sparse(A);
    // spilu_iludplot(A);
    // 
    // // See on a 225-by-225 sparse matrix
    // path = spilu_getpath (  );
    // filename = fullfile(path,"tests","matrices","pde225.mtx");
    // A=mmread(filename);
    // [data,hLfil,hDrop] = spilu_iludplot(A);
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

    function [pNorm,pNnz] = iludCompute(L,U)
        pNorm = norm(A-L*U,"inf")/norm(A,"inf");            
        pNnz = nnz(L) + nnz(U)
    endfunction

    function [pNorm,pNnz] = iludDecompAlpha(A,alpha)
        instr = "[L,U]=spilu_ilud(A,alpha)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = iludCompute(L,U)
        end
    endfunction

    function [pNorm,pNnz] = iludDecompDrop(A,drop)
        instr = "[L,U]=spilu_ilud(A,[],drop)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = iludCompute(L,U)
        end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_iludplot" , rhs , 1:2 )
    apifun_checklhs ( "spilu_iludplot" , lhs , 0:3 )
    // 
    // Get arguments
    A = varargin(1)
    N = apifun_argindefault ( varargin , 2 , 100 )
    //
    // Check type
    apifun_checktype ( "spilu_iludplot" , A , "A" , 1 , "sparse" )
    apifun_checktype ( "spilu_iludplot" , N , "N" , 2 , "constant" )
    //
    // Check size
    apifun_checksquare ( "spilu_iludplot" , A , "A" , 1 )
    apifun_checkscalar ( "spilu_iludplot" , N , "N" , 2 )
    //
    // Check content
    apifun_checkgreq ( "spilu_iludplot" , N , "N" , 2 , 1 )
    //
    // Proceed...

    //
    // Compute decomposition for various values of alpha
    alpha = linspace(0,1,N)'
    hAlpha = scf();
    [pNormAlpha,pNnzAlpha] = spilu_plotparameter(A,"alpha",alpha,list(iludDecompAlpha),%f)
    //
    // Compute decomposition for various values of drop
    drop = max(abs(A)) * logspace(-8,1,N)'
    hDrop = scf();
    [pNormDrop,pNnzDrop] = spilu_plotparameter(A,"drop",drop,list(iludDecompDrop),%f)
    hDrop.children(1).log_flags = "lnn"
    hDrop.children(2).log_flags = "lnn"
    //
    // Gather the results
    data = [alpha,pNormAlpha,pNnzAlpha,drop,pNormDrop,pNnzDrop]
endfunction
