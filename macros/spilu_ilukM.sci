// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) Yousef Saad
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [L, U] = spilu_ilukM(varargin) 
    // ILU preconditioning with level of fill-in of k (macro).
    // 
    // Calling Sequence
    // [L, U] = spilu_ilukM(A) 
    // [L, U] = spilu_ilukM(A,lfil) 
    //
    // Parameters
    // A : n-by-n sparse real matrix of doubles
    // lfil : a 1-by-1 matrix of doubles, integer value, positive, the fill-in parameter (default: lfil=floor(1+nnz(A)/n), i.e. the average number of nonzero elements of the matrix A by line). Entries whose levels-of-fill exceed lfil during the ILU process are dropped. 
    // L  : n-by-n sparse real matrix of doubles, lower triangular matrix
    // U : n-by-n sparse real matrix of doubles, upper triangular matrix
    //
    // Description
    // Builds an incomplete LU factorization of the sparse 
	// matrix <literal>A</literal>, 
	// that is, computes a lower triangular matrix <literal>L</literal> and 
	// an upper triangular matrix <literal>U</literal> such that 
	// 
	// <screen>
	// A &#8776; L*U
	// </screen>
    //
	// This function uses an ILU with level of fill-in of K (ILU(k)).
	//
	// If <literal>lfil==0</literal>, then spilu_ilukM produces the 
	// same output as spilu_ilu0M.
	//
    // This function is a macro.
    // The purpose of this function is to reproduce the spilu_iluk 
    // function for small sparse matrices with a simple script.
    // This function is a Scilab port of Youcef Saad's iluk
    // Matlab function.
    //
    // Examples
	// // A 6-by-6 matrix
	// A=[
	// -1.    3.    0.    0.    4.    0.  
	// 2.   -5.    0.    0.    0.    1.  
	// 0.    0.   -2.    3.    0.    0.  
	// 0.    0.    7.   -1.    0.    0.  
	// -3.    0.    0.    4.    6.    0.
	// 0.    5.    0.    0.   -7.    8. 
	// ];
	// A=sparse(A);
	// [L,U]=spilu_ilukM(A,lfil)
	// // See the norm of the residual matrix
	// norm(A-L*U)
	// // Configure level of fill
	// n = size(A,"r");
	// lfil=floor(1+nnz(A)/n);
	// [L,U]=spilu_ilukM(A,lfil)
	//
	// // Decompose a 237-by-237 sparse matrix.
    // path = spilu_getpath (  );
    // filename = fullfile(path,"tests","matrices","nos1.mtx");
    // A=mmread(filename);
    // [L,U]=spilu_ilukM(A,10);
    // norm(A-L*U)
    //
    // // To edit the code
    // edit spilu_ilukM
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    // Copyright (C) Yousef Saad
	
    [lhs, rhs] = argn()
    apifun_checkrhs ( "spilu_ilukM" , rhs , 1:2 )
    apifun_checklhs ( "spilu_ilukM" , lhs , 2:2 )
	//
	A = varargin(1)
	n = size(A,"r")
	lfil = apifun_argindefault ( varargin , 2 , floor(1+nnz(A)/n) )
    //
    // Check types
    apifun_checktype ( "spilu_ilukM" , A ,  "A" , 1 , "sparse" )
    //
    // Check size
    apifun_checksquare ( "spilu_ilukM" , A ,  "A" , 1 )

    n = size(A,1);
    L = speye(n,n);
    U = spzeros(n,n);
    levs = spzeros(n,n);
    //// NOTE: the code uses a shifted definition of lfil : 
    lfilp = lfil+1;
    //
    // Beginning of main loop.
    //
    for ii = 1:n
        //
        // Unpack row ii of A 
        //
        w = A(ii,:);
        jr = find(w);
        levii = spzeros(1,n);
        levii(jr) = 1;
        jj = 0;
        while (jj < ii)
            //
            // Select smallest column index among 
            // jw(k), k=jj+1, ..., lenl. 
            //
            jj = jj+min(find(w(jj+1:n)));
            if (jj >= ii) then
                break;
            end 
            jlev = levii(jj);
            if (jlev <= lfilp) then
                //  
                // Combine current row and  row jj.  
                //  
                [ijr,v]=spget(U(jj,:))
                jr = ijr(:,2)
                fact = w(jj)/v(1);
                for k=1:length(jr) 
                    j = jr(k);
                    temp = levs(jj,j) + jlev;
                    if (levii(j) == 0 ) then
                        if (temp <= lfilp) then
                            w(j) =  - fact*v(k);
                            levii(j) = temp;
                        end
                    else 
                        w(j) = w(j) - fact*v(k);
                        levii(j) = min(levii(j),temp);
                    end
                end 
                w(jj) = fact;
            end
        end
        [ijw,s]=spget(w)
        jw = ijw(:,2)
        [ijl,ll]=spget(levii)
        for k=1:length(jw)
            j = jw(k);
            if (j < ii) then
                L(ii,j) = s(k);
            else 
                U(ii,j) = s(k);
                levs(ii,j) = ll(k);
            end
        end
    end
endfunction


