// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [L,U,perm] = spilu_iluhub(A,m,varargin)
    // A generic hub for various incomplete LU algorithms.
    // 
    // Calling Sequence
    //   [L,U,perm] = spilu_iluhub(A,m) 
    //   [L,U,perm] = spilu_iluhub(A,m,a1) 
    //   [L,U,perm] = spilu_iluhub(A,m,a1,a2) 
    //   [L,U,perm] = spilu_iluhub(A,m,a1,a2,...) 
    //
    // Parameters
    // A : n-by-n sparse real matrix of doubles
    // m : 1-by-1 matrix of strings, the ILU method. Available methods are m = "ilu0", "ilud", "iludp", "iluk", "ilut", "ilutp", "milu0"
    // a1,a2,... : optional parameters of the ILU method
    // L : n-by-n sparse real matrix of doubles, lower triangular matrix
    // U : n-by-n sparse real matrix of doubles, upper triangular matrix
    // perm : a 1-by-n dense real matrix of doubles, integer value, the permutation arrays
    //
    // Description
    // Builds an incomplete LU factorization of the sparse matrix <literal>A</literal>, 
    // with the method <literal>m</literal>. 
    //
    // The extra arguments <literal>a1, a2, ...</literal> are automatically 
    // passed to the specific ILU function: see in the specific 
    // help page for the meaning of the parameters.
    //
    // If the specific algorithm does not produce a 
    // permutation vector <literal>perm</literal>, then 
    // the permutation is the identity. 
    //
    // Examples
    // // A small 3-by-3 matrix
    // // nnz(A)=7
    // A = [
    // 1 2 0
    // 3 4 5
    // 0 6 7
    // ];
    // A = sparse(A);
    // [L,U,perm]=spilu_iluhub(A,"ilu0")
	//
	// // Test ilud
	// alph = 0.1;
	// drop = 0.001;
    // [L,U,perm]=spilu_iluhub(A,"ilud",alph,drop)
    //
    // // Test all methods
	// marray = spilu_iluhubavail();
    // for m = marray
    //     mprintf("Method: %s\n",m);
    //     [L,U,perm]=spilu_iluhub(A,m);
    //     mprintf("    nnz(LU)= %d\n",nnz(L)+nnz(U));
    // end
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "spilu_iluhub" , rhs , 2:6 )
    apifun_checklhs ( "spilu_iluhub" , lhs , 3:3 )
    //
    // Check types
    apifun_checktype ( "spilu_iluhub" , A ,  "A" , 1 , "sparse" )
    apifun_checktype ( "spilu_iluhub" , m ,  "m" , 2 , "string" )
    //
    // Check size
    apifun_checksquare ( "spilu_iluhub" , A ,  "A" , 1 )
    apifun_checkscalar ( "spilu_iluhub" , m ,  "m" , 2 )
    //
    // Check content
    m_available = ["ilu0" "ilud" "iludp" "iluk" "ilut" "ilutp" "milu0"]
    apifun_checkoption ( "spilu_iluhub" , m ,  "m" , 2 , m_available )
    //
    // Proceed...
    n = size(A,"r")
    select m
    case "ilu0"
        [L,U]=spilu_ilu0(A,varargin(:));
        perm = 1:n;
    case "ilud" then
        [L,U]=spilu_ilud(A,varargin(:));
        perm = 1:n;
    case "iludp" then
        [L,U,perm]=spilu_iludp(A,varargin(:));
    case "iluk" then
        [L,U]=spilu_iluk(A,varargin(:));
        perm = 1:n;
    case "ilut" then
        [L,U]=spilu_ilut(A,varargin(:));
        perm = 1:n;
    case "ilutp" then
        [L,U,perm]=spilu_ilutp(A,varargin(:));
    case "milu0" then
        [L,U]=spilu_milu0(A,varargin(:));
        perm = 1:n;
    else
        error(msprintf("%s: Unknown method %s","spilu_iluhub",m))
    end
endfunction
