// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [data,hLfil,hDrop] = spilu_ilutplot(varargin)
    // Plots the sensitivity of ILUT for A.
    // 
    // Calling Sequence
    //   spilu_ilutplot(A)
    //   data = spilu_ilutplot(A)
    //   data = spilu_ilutplot(A,N)
    //   [data,hLfil] = spilu_ilutplot(...)
    //   [data,hLfil,hDrop] = spilu_ilutplot(...)
    //
    // Parameters
    // A: a n-by-n sparse matrix
    // N: a 1-by-1 matrix of doubles, integer value, the number of points in the plot (default N = 100).
    // data: a N-by-6 matrix of doubles, the values of the parameters
    // hLfil: a graphics handle, the lfil plot
    // hDrop: a graphics handle, the drop plot
    //
    // Description
    // For one particular matrix, plot norm(A-L*U)/norm(A) and nnz(L)+nnz(U), depending on 
    // lfil and drop for the ILUT method.
    //
    // The columns of the array data are:
    // <itemizedlist>
    //     <listitem><para>
    //         data(:,1) : the values of lfil
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,2) : the values of norm(A-L*U)/norm(A) with respect to lfil
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,3) : the values of nnz(L)+nnz(U) with respect to lfil
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,4) : the values of drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,5) : the values of norm(A-L*U)/norm(A) with respect to drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,6) : the values of nnz(L)+nnz(U) with respect to drop
    //     </para></listitem>
    // </itemizedlist>
    // 
    // If the decomposition does not work for one value of the parameter, 
    // then norm(A-L*U)/norm(A) and nnz(L)+nnz(U) are set to Infinity.
    // 
    // Examples
    // A = [
    //   10.     -6.    -1.     0.     3.  
    //   -6.     34.     0.    10.     0.  
    //   -1.      0.     1.     0.     0.  
    //    0.     10.     0.    10.     0.  
    //    3.      0.     0.     0.     3.  
    // ];
    // A = sparse(A);
    // spilu_ilutplot(A);
    //
    // // See on a 225-by-225 sparse matrix
    // path = spilu_getpath (  );
    // filename = fullfile(path,"tests","matrices","pde225.mtx");
    // A=mmread(filename);
    // [data,hLfil,hDrop] = spilu_ilutplot(A);
    // // The drop parameter is not sensitive:
    // delete(hDrop)
    // // Look at the lfil plot.
    // hLfil.children(1).data_bounds(2,1) = 20;
    // hLfil.children(2).data_bounds(2,1) = 20;
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

    function [pNorm,pNnz] = ilutDecompLfil(A,lfil)
        instr = "[L,U]=spilu_ilut(A,lfil)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            pNorm = norm(A-L*U,"inf")/norm(A,"inf");            
            pNnz = nnz(L) + nnz(U)
        end
    endfunction

    function [pNorm,pNnz] = ilutDecompDrop(A,drop)
        instr = "[L,U]=spilu_ilut(A,[],drop)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            pNorm = norm(A-L*U,"inf")/norm(A,"inf");            
            pNnz = nnz(L) + nnz(U)
        end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_ilutplot" , rhs , 1:2 )
    apifun_checklhs ( "spilu_ilutplot" , lhs , 0:3 )
    // 
    // Get arguments
    A = varargin(1)
    N = apifun_argindefault ( varargin , 2 , 100 )
    //
    // Check type
    apifun_checktype ( "spilu_ilutplot" , A , "A" , 1 , "sparse" )
    apifun_checktype ( "spilu_ilutplot" , N , "N" , 2 , "constant" )
    //
    // Check size
    apifun_checksquare ( "spilu_ilutplot" , A , "A" , 1 )
    apifun_checkscalar ( "spilu_ilutplot" , N , "N" , 2 )
    //
    // Check content
    apifun_checkgreq ( "spilu_ilutplot" , N , "N" , 2 , 1 )
    //
    // Proceed...
    //
    // Compute decomposition for various values of lfil
    n = size(A,"r")
    lfil = floor(linspace(0,n,N)')
    hLfil = scf();
    [pNormLfil,pNnzLfil] = spilu_plotparameter(A,"lfil",lfil,list(ilutDecompLfil),%t)
    //
    // Compute decomposition for various values of drop
    drop = max(abs(A)) * logspace(-16,0,N)'
    hDrop = scf();
    [pNormDrop,pNnzDrop] = spilu_plotparameter(A,"drop",drop,list(ilutDecompDrop),%t)
    h = gcf()
    h.children(1).log_flags = "lnn"
    h.children(2).log_flags = "lnn"
    //
    // Gather the results
    data = [...
    lfil,pNormLfil,pNnzLfil,..
    drop,pNormDrop,pNnzDrop...
    ]
endfunction
