// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function P = spilu_permVecToMat(perm)
    // Convert a permutation vector into matrix.
    // 
    // Calling Sequence
    //   P = spilu_permVecToMat(perm)
    //
    // Parameters
    //   perm : a 1-by-n full matrix of doubles, integer value, positive, the permutation vector. Each entry perm(i) should be in the set {1,2,...,n}.
    //   P : a n-by-n sparse matrix of doubles, the permutation matrix
    //
    // Description
    // Convert a permutation vector into a sparse permutation matrix.
    //
    // Examples
    // perm = [5,2,3,4,1,6]
    // P = spilu_permVecToMat(perm);
    // full(P)
    // Pexpected = [
    //     0.    0.    0.    0.    1.    0.  
    //     0.    1.    0.    0.    0.    0.  
    //     0.    0.    1.    0.    0.    0.  
    //     0.    0.    0.    1.    0.    0.  
    //     1.    0.    0.    0.    0.    0.  
    //     0.    0.    0.    0.    0.    1.  
    // ];
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)

	[lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_permVecToMat" , rhs , 1 )
    apifun_checklhs ( "spilu_permVecToMat" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "spilu_permVecToMat" , perm , "perm" , 1 , "constant" )
	//
	// Check size
    apifun_checkvector ( "spilu_permVecToMat" , perm , "perm" , 1 )
	//
	// Check content
	apifun_checkgreq ( "spilu_permVecToMat" , perm , "perm" , 1 , 1 )
    n=size(perm,"*");
	apifun_checkloweq ( "spilu_permVecToMat" , perm , "perm" , 1 , n )
	//
	// Proceed...
    P=spzeros(n,n);
    for i=1:n
        P(perm(i),i)=1;
    end
endfunction

