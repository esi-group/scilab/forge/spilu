<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from spilu_ilukM.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="spilu_ilukM" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>spilu_ilukM</refname><refpurpose>ILU preconditioning with level of fill-in of k (macro).</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [L, U] = spilu_ilukM(A)
   [L, U] = spilu_ilukM(A,lfil)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A :</term>
      <listitem><para> n-by-n sparse real matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>lfil :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the fill-in parameter (default: lfil=floor(1+nnz(A)/n), i.e. the average number of nonzero elements of the matrix A by line). Entries whose levels-of-fill exceed lfil during the ILU process are dropped.</para></listitem></varlistentry>
   <varlistentry><term>L  :</term>
      <listitem><para> n-by-n sparse real matrix of doubles, lower triangular matrix</para></listitem></varlistentry>
   <varlistentry><term>U :</term>
      <listitem><para> n-by-n sparse real matrix of doubles, upper triangular matrix</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Builds an incomplete LU factorization of the sparse
matrix <literal>A</literal>,
that is, computes a lower triangular matrix <literal>L</literal> and
an upper triangular matrix <literal>U</literal> such that
   </para>
   <para>
<screen>
A &amp;#8776; L*U
</screen>
   </para>
   <para>
This function uses an ILU with level of fill-in of K (ILU(k)).
   </para>
   <para>
If <literal>lfil==0</literal>, then spilu_ilukM produces the
same output as spilu_ilu0M.
   </para>
   <para>
This function is a macro.
The purpose of this function is to reproduce the spilu_iluk
function for small sparse matrices with a simple script.
This function is a Scilab port of Youcef Saad's iluk
Matlab function.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// A 6-by-6 matrix
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
[L,U]=spilu_ilukM(A,lfil)
// See the norm of the residual matrix
norm(A-L*U)
// Configure level of fill
n = size(A,"r");
lfil=floor(1+nnz(A)/n);
[L,U]=spilu_ilukM(A,lfil)

// Decompose a 237-by-237 sparse matrix.
path = spilu_getpath (  );
filename = fullfile(path,"tests","matrices","nos1.mtx");
A=mmread(filename);
[L,U]=spilu_ilukM(A,10);
norm(A-L*U)

// To edit the code
edit spilu_ilukM

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) Yousef Saad</member>
   </simplelist>
</refsection>
</refentry>
