<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 *
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2011 - NII - Benoit Goepfert
 * Copyright (C) 2005 - INRIA - Sage Group (IRISA)
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry
  xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:db="http://docbook.org/ns/docbook"
  version="5.0-subset Scilab"
  xml:lang="en"
  xml:id="spilu_ilud">
  <refnamediv>
    <refname>spilu_ilud</refname>
    <refpurpose> sparse ILU factorization with single dropping and diagonal compensation  </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [L,U]=spilu_ilud(A)
      [L,U]=spilu_ilud(A,alph)
      [L,U]=spilu_ilud(A,alph,drop)
    </synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>A  </term>
        <listitem>
          <para>
            a n-by-n sparse real matrix of doubles
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>alph  </term>
        <listitem>
          <para>
            a 1-by-1 matrix of doubles, positive, diagonal compensation parameter (default: 0.5)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>drop  </term>
        <listitem>
          <para>
            a 1-by-1 matrix of doubles, positive, threshold parameter for dropping 
            small terms in the factorization (default: <literal>drop=0.001*max(abs(A))</literal>)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>L  </term>
        <listitem>
          <para>
            a n-by-n sparse real matrix of doubles, lower triangular
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>U  </term>
        <listitem>
          <para>
            a n-by-n sparse real matrix of doubles, upper triangular
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      Builds an incomplete LU factorization of the sparse matrix <literal>A</literal>, 
	  that is, computes a lower triangular matrix <literal>L</literal> and 
	  an upper triangular matrix <literal>U</literal> such that 
    </para>
    <para>
      <screen>
        A &#8776; L*U
      </screen>
    </para>
    <para>
      All diagonal elements of the input matrix must be nonzero.
    </para>
    <para>
      Any optional input argument equal to the empty matrix <literal>[]</literal> is replaced
      by its default value.
    </para>
    <para>
      This routine computes the ILU factorization with standard threshold
      dropping.
    </para>

    <para>
      There is no control on memory size required for the factors as is
      done in ILUT. This routines computes also various diagonal compensation 
	  ILU's such MILU. These are defined through the 
	  parameter <literal>alph</literal>.
    </para>
    <para>
      The parameter <literal>alph</literal> is the diagonal compensation parameter.
      The term
    </para>
    <para>
      <screen>
alph*(sum of all dropped out elements in a given row)
      </screen>
    </para>
    <para>
      is added to the diagonal element of <literal>U</literal> of the factorization.
      Thus:
      <itemizedlist>
        <listitem>
          <para>
            <literal>alph</literal>=0  : ILU with threshold,
          </para>
        </listitem>
        <listitem>
          <para>
            <literal>alph</literal>=1  : MILU with threshold.
          </para>
        </listitem>
      </itemizedlist>
    </para>
    <para>
      The <literal>drop</literal> parameter is the threshold parameter for dropping small
      terms in the factorization.
      During the elimination, a term <literal>A(i,j)</literal> is dropped whenever
    </para>
    <para>
      <screen>
drop * (weighted norm of A(i,:)) &gt; abs(A(i,j))
      </screen>
    </para>
    <para>
      Here weighted norm = 1-norm / (number of nnz elements in the row).
    </para>
    <para>
      Hence, a smaller value of <literal>drop</literal> (e.g. <literal>drop=0.</literal>) leads to a better 
	  decomposition of the matrix, but also leads to a larger number of nonzeros 
	  in the matrices <literal>L</literal> and <literal>U</literal>.
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[

// Incomplete factorization of a 5-by-5 sparse matrix
// nnz(A)=11
A=[
    18.    0.    -6.     0.     3.  
    0.     16.    0.     8.     0.  
   -6.     0.     36.    0.     0.  
    0.     8.     0.     16.    0.  
    3.     0.     0.     0.     6.  
];
A=sparse(A);
// With default options
[L,U]=spilu_ilud(A); 
// Check the quality of the decomposition
[norm(A-L*U,"inf")/norm(A,"inf") nnz(L)+nnz(U)]
// Configure alpha
[L,U]=spilu_ilud(A,1.); 
[norm(A-L*U,"inf")/norm(A,"inf") nnz(L)+nnz(U)]
// Configure alpha and drop
[L,U]=spilu_ilud(A,1.,0.); 
[norm(A-L*U,"inf")/norm(A,"inf") nnz(L)+nnz(U)]

// Incomplete factorization of a 960-by-960 sparse matrix
// nnz(A)=15844
path = spilu_getpath (  );
filename = fullfile(path,"tests","matrices","nos3.mtx");
A=mmread(filename);
n=size(A,1);
b=ones(n,1);
alph=0;
drop=0;
[L,U]=spilu_ilud(A,alph,drop);
x=U\(L\b);
norm(A*x-b)
 ]]>
    </programlisting>
  </refsection>
  <refsection>
    <title>Authors</title>
    <para>Copyright (C) 2011 - DIGITEO - Michael Baudin</para>
    <para>Copyright (C) 2011 - NII - Benoit Goepfert </para>
    <para>Copyright (C) 2005 - INRIA - Sage Group (IRISA)</para>
    <para>Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory - Youcef Saad</para>
  </refsection>
</refentry>
